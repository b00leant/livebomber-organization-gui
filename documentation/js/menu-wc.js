'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">live-bomber-ng documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' : 'data-target="#xs-components-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' :
                                            'id="xs-components-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' }>
                                            <li class="link">
                                                <a href="components/AboutComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AboutComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ActionCardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ActionCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ActionVideoCardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ActionVideoCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BestActionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BestActionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BestMatchesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BestMatchesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CategoriesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CategoriesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FoldingBannerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FoldingBannerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HorizontalScrollerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HorizontalScrollerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InfoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InfoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LeagueComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LeagueComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LeftEntryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LeftEntryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LineComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LineComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MainComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MainComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MatchCardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MatchCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NewsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NewsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PlayerCardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlayerCardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PlayerComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PlayerComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProfileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProfileHeaderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProfileHeaderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ProgressComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProgressComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RightEntryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RightEntryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SelectionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SelectionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SettingsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SettingsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShareBottomSheet.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ShareBottomSheet</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignInComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SignInComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignInDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SignInDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignUpComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SignUpComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SingleVideoPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SingleVideoPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SplashButtonsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SplashButtonsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SplashScreenComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SplashScreenComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SupportComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SupportComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TimelineListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimelineListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TimelineStartEntryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimelineStartEntryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/VideoModalComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">VideoModalComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' : 'data-target="#xs-directives-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' :
                                        'id="xs-directives-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' }>
                                        <li class="link">
                                            <a href="directives/ClickOutsideDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">ClickOutsideDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/ImagePreloadDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">ImagePreloadDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/LoadingImageDirective.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoadingImageDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' : 'data-target="#xs-pipes-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' :
                                            'id="xs-pipes-links-module-AppModule-161890d7f2cb5940b902216926ef9199"' }>
                                            <li class="link">
                                                <a href="pipes/FilterPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FilterPipe</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/OrgFilter.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OrgFilter</a>
                                            </li>
                                            <li class="link">
                                                <a href="pipes/TruncatePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TruncatePipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppServerModule.html" data-type="entity-link">AppServerModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' : 'data-target="#xs-components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' :
                                            'id="xs-components-links-module-AppServerModule-7af5a252e97af6fdb059ab754264a081"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MaterialModule.html" data-type="entity-link">MaterialModule</a>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Action.html" data-type="entity-link">Action</a>
                            </li>
                            <li class="link">
                                <a href="classes/ActionCardData.html" data-type="entity-link">ActionCardData</a>
                            </li>
                            <li class="link">
                                <a href="classes/ActionInfo.html" data-type="entity-link">ActionInfo</a>
                            </li>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                            <li class="link">
                                <a href="classes/Category.html" data-type="entity-link">Category</a>
                            </li>
                            <li class="link">
                                <a href="classes/Classification.html" data-type="entity-link">Classification</a>
                            </li>
                            <li class="link">
                                <a href="classes/Group.html" data-type="entity-link">Group</a>
                            </li>
                            <li class="link">
                                <a href="classes/GroupContainer.html" data-type="entity-link">GroupContainer</a>
                            </li>
                            <li class="link">
                                <a href="classes/League.html" data-type="entity-link">League</a>
                            </li>
                            <li class="link">
                                <a href="classes/LeagueCalendar.html" data-type="entity-link">LeagueCalendar</a>
                            </li>
                            <li class="link">
                                <a href="classes/LeagueDay.html" data-type="entity-link">LeagueDay</a>
                            </li>
                            <li class="link">
                                <a href="classes/Livestory.html" data-type="entity-link">Livestory</a>
                            </li>
                            <li class="link">
                                <a href="classes/Match.html" data-type="entity-link">Match</a>
                            </li>
                            <li class="link">
                                <a href="classes/MyHammerConfig.html" data-type="entity-link">MyHammerConfig</a>
                            </li>
                            <li class="link">
                                <a href="classes/Organization.html" data-type="entity-link">Organization</a>
                            </li>
                            <li class="link">
                                <a href="classes/Person.html" data-type="entity-link">Person</a>
                            </li>
                            <li class="link">
                                <a href="classes/Player.html" data-type="entity-link">Player</a>
                            </li>
                            <li class="link">
                                <a href="classes/Team.html" data-type="entity-link">Team</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/APIService.html" data-type="entity-link">APIService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CopypasteService.html" data-type="entity-link">CopypasteService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LBAuthService.html" data-type="entity-link">LBAuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ModalEventService.html" data-type="entity-link">ModalEventService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SettingsService.html" data-type="entity-link">SettingsService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/APIInterceptor.html" data-type="entity-link">APIInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/ServerData.html" data-type="entity-link">ServerData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/SignInData.html" data-type="entity-link">SignInData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/VideoModalData.html" data-type="entity-link">VideoModalData</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});