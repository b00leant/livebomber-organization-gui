import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Action } from '../models/action';
import { Observable } from 'rxjs';
import { APIService } from '../services/api.service';
import { first, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { isPlatformServer } from '@angular/common';
import { makeStateKey, TransferState } from '@angular/platform-browser';


@Injectable()
export class SingleVideoResolver implements Resolve<Action> {

    constructor(
        private _api: APIService,
        @Inject(PLATFORM_ID) private platformId,
        private transferState:TransferState
        ) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Action> {

        const videoId = route.params['id'];

        const ACTION_KEY = makeStateKey<Action>('action-' + videoId);

        if (this.transferState.hasKey(ACTION_KEY)) {

            const action = this.transferState.get<Action>(ACTION_KEY, null);

            this.transferState.remove(ACTION_KEY);

            return of(action);
        }
        else {
            return this._api.getActionForResolver(videoId)
            .pipe(
                first(),
                tap(action => {

                    if (isPlatformServer(this.platformId)) {
                        this.transferState.set(ACTION_KEY, action);
                    }

                })
                );
        }
    }
}