import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Player } from '../models/player';
import { Observable } from 'rxjs';
import { APIService } from '../services/api.service';
import { first, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { isPlatformServer } from '@angular/common';
import { makeStateKey, TransferState } from '@angular/platform-browser';


@Injectable()
export class PlayerResolver implements Resolve<Player> {

    constructor(
        private _api: APIService,
        @Inject(PLATFORM_ID) private platformId,
        private transferState:TransferState
        ) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Player> {

        const playerId = route.params['player_id'];

        const PLAYER_KEY = makeStateKey<Player>('action-' + playerId);

        if (this.transferState.hasKey(PLAYER_KEY)) {

            const action = this.transferState.get<Player>(PLAYER_KEY, null);

            this.transferState.remove(PLAYER_KEY);

            return of(action);
        }
        else {
            return this._api.getPlayerForResolver(playerId)
            .pipe(
                first(),
                tap(player => {

                    if (isPlatformServer(this.platformId)) {
                        this.transferState.set(PLAYER_KEY, player);
                    }

                })
                );
        }
    }
}