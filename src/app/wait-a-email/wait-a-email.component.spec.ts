import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitAEmailComponent } from './wait-a-email.component';

describe('WaitAEmailComponent', () => {
  let component: WaitAEmailComponent;
  let fixture: ComponentFixture<WaitAEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitAEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitAEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
