import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { LBAuthService } from './services/lbauth.service';
import { Router } from '@angular/router';
import { UserService } from './services/user.service'
import { map } from 'rxjs/operators'
import { APIService } from './services/api.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private cookie:CookieService,
    private api:APIService,
    private auth: LBAuthService, 
    private router: Router,
    private user: UserService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let path = next.url[0].path;
    if(this.auth.isLoggedIn && path != 'verifyemail' && path !='signup' && path != 'login' && path != 'welcome') {
      return true
    }
    else if(next.url.length==1 && 
      (next.url[0].path =='welcome'||
        next.url[0].path =='signup'||
        next.url[0].path =='login'||
        next.url[0].path == 'verifyemail')){  
      if(localStorage.getItem('lbac2k19')!=null){
        let token = localStorage.getItem('lbac2k19');
        return this.api.validateToken(token).pipe(map(val=>{
          if(val.authenticated){
            this.auth.setLoggedIn(true)
            this.api.getUser(val.uid).subscribe(user=>{
              this.auth.setUser(user.nome,user.cognome,user.id,user.email);
            })
            this.router.navigate(['/channel/'+val.orgid+'/top']);
            return true;
          }else{
            return true;
          }
        }));
      }else{
        return true;
      }
    }
    else if(next.url.length==1 && next.url[0].path =='selection'){
      if(localStorage.getItem('lbac2k19')!=null){
        return true;
        let token = localStorage.getItem('lbac2k19');
        return this.api.validateToken(token).pipe(map(val=>{
          if(val.authenticated){
            this.auth.setLoggedIn(true)
            this.api.getUser(val.uid).subscribe(user=>{
              this.auth.setUser(user.nome,user.cognome,user.id,user.email);
            })
            return true;
          }else{
            this.router.navigate(['welcome'])
            return false;
          }
        }));
      }else if(localStorage.getItem('lblu2k19')!=null){
        return true;
      }else{
        this.router.navigate(['welcome'])
        return false;
      }
    }else{
      if(localStorage.getItem('lbac2k19')!=null){
        let token = localStorage.getItem('lbac2k19');
        return this.api.validateToken(token).pipe(map(val=>{
          if(val.authenticated){
            this.auth.setLoggedIn(true)
            this.api.getUser(val.uid).subscribe(user=>{
              this.auth.setUser(user.nome,user.cognome,user.id,user.email);
            })
            return true;
          }else{
            this.router.navigate(['welcome'])
            return false;
          }
        }));
      }else{
        this.router.navigate(['welcome'])
        return false;
      }
    }
  }
}