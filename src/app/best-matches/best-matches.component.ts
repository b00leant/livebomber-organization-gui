import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator'
import { Location } from '@angular/common';
import { Match } from '../models/match';


@Component({
	selector: 'app-best-matches',
	templateUrl: './best-matches.component.html',
	styleUrls: ['./best-matches.component.scss']
})
export class BestMatchesComponent implements OnInit {

	matches: Match[] = [
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"}
	];

	loaded:boolean = false;

	constructor(private _loc:Location, private _api:APIService,private _ar:ActivatedRoute) { }

	goBack(){
		this._loc.back();
	}

	updateProductsDisplayedInPage(e: PageEvent){

	}

	getBestActions(id:string,limit:number){
		return this._api.getBestActions(id,limit).subscribe(a=>{
			//this.matches = a;
			this.loaded = true;
		});
	}


	ngOnInit() {
		this.loaded = true;
		let id = this._ar.snapshot.paramMap.get('id');
		//this.getBestActions(id,12);
	}

}
