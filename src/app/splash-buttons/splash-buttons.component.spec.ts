import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SplashButtonsComponent } from './splash-buttons.component';

describe('SplashButtonsComponent', () => {
  let component: SplashButtonsComponent;
  let fixture: ComponentFixture<SplashButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SplashButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SplashButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
