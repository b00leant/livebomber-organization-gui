import { Component, OnInit } from '@angular/core';
import { AuthService } from "angularx-social-login";
import { DomSanitizer } from '@angular/platform-browser'
import { MatIconRegistry} from '@angular/material/icon'
import { Router, NavigationEnd } from '@angular/router';
import { LBAuthService } from '../services/lbauth.service';

@Component({
	selector: 'app-splash-buttons',
	templateUrl: './splash-buttons.component.html',
	styleUrls: ['./splash-buttons.component.scss']
})
export class SplashButtonsComponent implements OnInit {

	constructor(
		private _lbAuth:LBAuthService,
		private _dom_san:DomSanitizer,
		private mat_reg:MatIconRegistry,
		private _ro:Router) {
		this.mat_reg.addSvgIcon('facebook-icon',this._dom_san.bypassSecurityTrustResourceUrl('../assets/logofb.svg'));

		_ro.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
			}
		});
	}


	signInWithFB(): void {
		this._lbAuth.signInWithFB();
	}	



	ngOnInit() {
	}

}
