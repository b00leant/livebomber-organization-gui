import { Component, OnInit,ViewChild } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Organization } from '../models/organization';
import { APIService } from '../services/api.service';
import { SettingsService } from '../services/settings.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import { MatStepper, MatStep} from '@angular/material/stepper';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { LBAuthService } from '../services/lbauth.service';

@Component({
	selector: 'app-selection',
	templateUrl: './selection.component.html',
	styleUrls: ['./selection.component.scss'],
	
})
export class SelectionComponent implements OnInit {

	black_list:string[] = [
	"test",
	"benvenuto",
	"prova 1",
	"asp demo",
	"lorenzo",
	"test-multi"
	]

	searchText:string =""

	@ViewChild('orgsTable') private orgsTable: MatTable<Organization>;
	@ViewChild('stepper') private stepper: MatStepper;
	@ViewChild('step1') private step1: MatStep;
	@ViewChild('step2') private step2: MatStep;

	columnsToDisplay = ['img','title'];
	expandedElement: Organization | null;

	lightUser:string="";

	orgid:string="";
	orgs: Organization[] = [];
	sports: string[] = [
	"Hockey",
	"Calcio"
	];
	isLinear = false;

	loaded:boolean =false;

	orgs$: Observable<Organization[]>;
	selectedOrg: Organization;
	selectedOrgURL:string;
	selectedOrgTitle:string ="";

	constructor(
		private _auth:LBAuthService,
		private _cookieService:CookieService,
		private _formBuilder: FormBuilder,
		private _api: APIService,
		private _settings: SettingsService,
		private _ro:Router) {}

	isEmptyObject(obj) {
		return (obj && (Object.keys(obj).length === 0));
	}

	applyFilter(filterValue: string) {
		//this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	isBlackListed(s:string){
		for(let t in this.black_list){
			if(this.black_list[t].includes(s.toLowerCase())){
				return true;
			}
		}
		return false;
	}

	selectOrganization(org: Organization){
		this.step1.completed = true;
		this.stepper.next();
		this.selectedOrg = org;
		this.orgid= org.id;
		this.selectedOrgTitle = org.title;
		this.selectedOrgURL = "https://league.livebomber.com/public/uploads/tornei/"+org.id+".png";

	}

	ngOnInit() {
		this.lightUser = localStorage.getItem('lblu2k19');
		localStorage.removeItem('lblu2k19')
		this.getAllOrgs()
	}

	confirm() {
		this.step2.completed = true;
		this._settings.setOrganization(this.selectedOrg);
		return this._api.getToken(this._auth.id,this.selectedOrg.id).subscribe(res=>{
			console.log(res);
			if(res.authenticated){
				//this._cookieService.set( 'lbac2k19',res.token);
				localStorage.setItem('lbac2k19',res.token);
				this._auth.setLoggedIn(true)
				this._ro.navigate(['/toapp'],{ queryParams: { uid: "fake", orgid: this.selectedOrg.id } });
			}
		});   
	}



	getAllOrgs() {
		return this._api.getAllOrgs().subscribe(data => {
			this.orgs = data;
			this.loaded = true;
		});
	}
}

