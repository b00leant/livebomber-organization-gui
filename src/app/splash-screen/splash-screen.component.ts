import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from '../animations';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry} from '@angular/material/icon';
import { AuthService } from "angularx-social-login";
import { Router, NavigationEnd } from '@angular/router';
import { LBAuthService } from '../services/lbauth.service';
@Component({
	selector: 'app-splash-screen',
	templateUrl: './splash-screen.component.html',
	styleUrls: ['./splash-screen.component.scss'],
	animations: [slideInAnimation]

})
export class SplashScreenComponent implements OnInit {

		constructor(
		private _lbAuth:LBAuthService,
		private _dom_san:DomSanitizer,
		private mat_reg:MatIconRegistry,
		private _ro:Router) {
		this.mat_reg.addSvgIcon('facebook-icon',this._dom_san.bypassSecurityTrustResourceUrl('../assets/logofb.svg'));
		mat_reg.addSvgIcon('livebomber-icon',_dom_san.bypassSecurityTrustResourceUrl('../assets/logolb_alt.svg'));
		_ro.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
			}
		});
	}


	signInWithFB(): void {
		this._lbAuth.signInWithFB();
	}	



	logolb(){
		return "url(../assets/logolb.svg)";
	}

	ngOnInit() {
	}

	prepareRoute(outlet: RouterOutlet) {
		return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
	}

}
