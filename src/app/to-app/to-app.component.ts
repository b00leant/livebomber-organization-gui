import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SettingsService } from '../services/settings.service';
import { APIService } from '../services/api.service';

@Component({
	selector: 'app-to-app',
	templateUrl: './to-app.component.html',
	styleUrls: ['./to-app.component.css']
})
export class ToAppComponent implements OnInit {

	orgid:string="";
	uid:string="";

	constructor(private _api:APIService,private _settings: SettingsService, private _ro:Router,private _ar:ActivatedRoute) {
		_ro.events.subscribe(event => {
			if (event instanceof NavigationEnd && _ro.url.includes('toapp')) {
				
			}
		});
	}

	setOrgAndRoute(id:string){
		return this._api.getOrg(id).subscribe(org=>{
			this._settings.setOrganization(org[0]);
			this._ro.navigate(['/channel',org[0].id ,'top']);
		});
	}

	ngOnInit() {
		if(this._ro.url.includes('toapp')){
			this.orgid = this._ar.snapshot.queryParamMap.get('orgid');
			this.uid = this._ar.snapshot.queryParamMap.get('uid');
			if(this.uid =="" ){
				this._ro.navigate(['/splash']);
			}
			if(this.orgid == ""){
				this._ro.navigate(['/selection'])
			}else{
				if(this._ar.snapshot.queryParamMap.get('token')=="Ap39kd!eie"){
					//get user session
				}
				console.log("sto navigando dentro toapp");

				this.setOrgAndRoute(this.orgid);
			}
		}
	}
}
