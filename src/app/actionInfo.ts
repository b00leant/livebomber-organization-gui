export class ActionInfo{
	type?:string;
	url:string;
	views?:string;
	date?:string;
}