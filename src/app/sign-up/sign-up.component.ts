import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, Validators, FormGroup,FormBuilder, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { APIService } from '../services/api.service';
import { SignUpFbResponse } from '../models/fb-sign-up'

@Component({
	selector: 'app-sign-up',
	templateUrl: './sign-up.component.html',
	styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

	signupGroup: FormGroup;
	minPw = 8;
	confirmPassword;
	valid:boolean = false;

	constructor(
		private _api:APIService,
		private _ro:Router,
		private _loc:Location,
		private _formBuilder:FormBuilder) { }

	ngOnInit() {
		this.signupGroup = this._formBuilder.group({
			fname: ['',[Validators.required]],
			lname:['',[Validators.required]],
			password: ['', [Validators.required, Validators.minLength(this.minPw)]],
			password2: ['', [Validators.required]],
			email: ['',[Validators.required, Validators.email]]
		}, {validator: passwordMatchValidator});
	}


	get fname() { return this.signupGroup.get('fname'); }
	get lname() { return this.signupGroup.get('lname'); }
	get password() { return this.signupGroup.get('password'); }
	get password2() { return this.signupGroup.get('password2'); }
	get email() { return this.signupGroup.get('email'); }

	goBack(){
		this._ro.navigate(['welcome','splash']);
	}

	signUpUser(event){
		console.log(
			this.fname.value+"\n"+
			this.lname.value+"\n"+
			this.password.value+"\n"+
			this.email.value);
		this._api.createEmailUser(
			this.fname.value,
			this.lname.value,
			this.email.value,
			this.password.value).subscribe(res =>{
				console.log(res);
				//console.log("Authentication: OK");
				let response:SignUpFbResponse = res;
				if(response.id!=""){
					//localStorage.setItem('lblu2k19',response[0].id);
					console.log(res);
					this._api.sendEmail(response.id).subscribe(res=>{
						console.log(res);
						this._ro.navigate(['waitamail'])
					})		
				}
			});
			//this._ro.navigate(['terms'])
		}

		getErrorMessage() {
			return this.email.hasError('required') ? 'You must enter a value' :
			this.email.hasError('email') ? 'Not a valid email' :
			'';
		}
		onPasswordInput() {
			if (this.signupGroup.hasError('passwordMismatch'))
				this.password2.setErrors([{'passwordMismatch': true}]);
			else
				this.password2.setErrors(null);
			if(this.signupGroup.valid){
				this.valid = true;
			}else{
				this.valid = false;
			}
		}
	}

	export const passwordMatchValidator: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
		if (formGroup.get('password').value === formGroup.get('password2').value)
			return null;
		else
			return {passwordMismatch: true};
	};