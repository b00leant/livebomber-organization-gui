import { Component, OnInit, Inject } from '@angular/core';
import { VideoModalData } from '../video-modal/videoModalData';
import { VideoModalComponent } from '../video-modal/video-modal.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Location } from '@angular/common';
import { APIService } from '../services/api.service';
import { Livestory } from '../models/livestory';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { DOCUMENT } from '@angular/platform-browser';
import { ActionCardData } from '../cards/action-card-data';

@Component({
  selector: 'app-timeline-list',
  templateUrl: './timeline-list.component.html',
  styleUrls: ['./timeline-list.component.scss']
})
export class TimelineListComponent implements OnInit {

  livestories: Livestory[] = [];

  sq1_id:string="";
  sq2_id:string="";

  sq1_img:string="";
  sq2_img:string="";

  default_img:string="https://league.livebomber.com/public/uploads/tornei/torneiPH.png";

  loaded:boolean = false;

  play_img_url:string="https://www.livebomber.com/assets/img/play_trasp.png";
  constructor(
    @Inject(DOCUMENT) private document: Document,
    public dialog: MatDialog,
    private _locaiton: Location,
    private _api: APIService,
    private _ar: ActivatedRoute,
    private _ro: Router) { }

  goBack(){
    this._locaiton.back();
  }

  openVideoModal(data:ActionCardData): void {
    const dialogRef = this.dialog.open(VideoModalComponent, {
      panelClass: 'lb-dialog-container',
      backdropClass: 'lb-backdrop-container',
      data: {
        url: "https://www.livebomber.com/up/"+data.url,
        type: data.type,
        nome_g:data.nome_g,
        cognome_g:data.cognome_g,
        livestory:data.livestory
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  getLivestory(id:string){
    this._api.getLivestory(id).subscribe(l => {
      this.loaded = true;
      this.livestories = l;
      this.sq1_id = l[0].sq1;
      this.sq2_id = l[0].sq2;
      this.sq1_img = "https://www.livebomber.com/up/uploads-sito/loghi/"+l[0].sq1+".png";
      this.sq2_img = "https://www.livebomber.com/up/uploads-sito/loghi/"+l[0].sq2+".png";
    });
  }
  
  ngOnInit() {

    this._ro.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.document.body.scrollTop = 0;
    });
    this.getLivestory(this._ar.snapshot.paramMap.get('id'));
  }

}
