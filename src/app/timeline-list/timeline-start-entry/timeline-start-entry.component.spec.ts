import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineStartEntryComponent } from './timeline-start-entry.component';

describe('TimelineStartEntryComponent', () => {
  let component: TimelineStartEntryComponent;
  let fixture: ComponentFixture<TimelineStartEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineStartEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineStartEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
