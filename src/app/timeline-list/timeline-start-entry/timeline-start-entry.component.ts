import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'timeline-start-entry',
  templateUrl: './timeline-start-entry.component.html',
  styleUrls: ['./timeline-start-entry.component.scss']
})
export class TimelineStartEntryComponent implements OnInit {

	@Input() fine:boolean;

  constructor() { }

  ngOnInit() {
  }

}
