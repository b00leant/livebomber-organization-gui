import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Livestory } from '../../models/livestory';
import { APIService } from '../../services/api.service';
import { ActionCardData } from '../../cards/action-card-data';

@Component({
  selector: 'timeline-right-entry',
  templateUrl: './right-entry.component.html',
  styleUrls: ['./right-entry.component.scss']
})
export class RightEntryComponent implements OnInit {

	@Output() modalAsked = new EventEmitter<ActionCardData>();
  @Input() livestory: Livestory;
  
  type:string;
  minuto:string;
  giocatore:string;

  nome_giocatore:string ="";
  cognome_giocatore:string="";

  typeLogoURL:string;

  loaded:boolean = false;

  tiro_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-tiro.png";
  parata_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-parata.png";
  goal_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-goal.png"
  cronometro_img_url:string="https://www.livebomber.com/public/img/cronometro.png";
  flop_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-flop.png"

  display:boolean = true;

  actionData:ActionCardData;

  askModal() {
    if(!this.getBoolForDisabled(this.livestory.isvideo)){
      this.modalAsked.emit(this.actionData);
    }
  }

  getBoolForDisabled(s:string){
    if(s == "1"){
      return false;
    }else{
      return true;
    }
  }

  getPlayer(id:string){
    return this._api.getPlayer(id).subscribe(g=>{
      //console.log(g);
      if(g){
        this.nome_giocatore = g.nome.toUpperCase();
        this.cognome_giocatore = g.cognome.toUpperCase();
        this.actionData.nome_g = g.nome.toUpperCase();
        this.actionData.cognome_g = g.cognome.toUpperCase();
        this.loaded = true;
      }else{
        this.display = false;
        this.loaded = true;
      }

      
    });
  }

  constructor(private _api:APIService) { }

  ngOnInit() {

    //console.log(this.livestory.id);

    this.actionData = {
        url: this.livestory.id + "/"+ this.livestory.id_video,
        type: this.livestory.tag_name,
        nome_g:"",
        cognome_g:"",
        livestory:this.livestory.id
      }

    //console.log("prova livestory");
    //console.log(this.livestory);
    this.type = this.livestory.tag_name;
    this.minuto = this.livestory.minuto;
    this.giocatore = this.livestory.giocatore;
    this.getPlayer(this.giocatore);

    switch (this.type) {
      case "goal":
      this.typeLogoURL = this.goal_img_url;
      break;
      
      case "tiro":  
      this.typeLogoURL = this.tiro_img_url;
      break;

      case "parata":
      this.typeLogoURL = this.parata_img_url;
      break;

      case "flop":
      this.typeLogoURL = this.flop_img_url;
      break;

      default:
      break;
    }
  }

}
