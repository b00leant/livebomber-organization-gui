import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RightEntryComponent } from './right-entry.component';

describe('RightEntryComponent', () => {
  let component: RightEntryComponent;
  let fixture: ComponentFixture<RightEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RightEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RightEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
