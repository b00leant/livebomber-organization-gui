import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftEntryComponent } from './left-entry.component';

describe('LeftEntryComponent', () => {
  let component: LeftEntryComponent;
  let fixture: ComponentFixture<LeftEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
