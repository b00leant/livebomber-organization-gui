import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Livestory } from '../../models/livestory';
import { APIService } from '../../services/api.service';

import { ActionCardData } from '../../cards/action-card-data';

@Component({
  selector: 'timeline-left-entry',
  templateUrl: './left-entry.component.html',
  styleUrls: ['./left-entry.component.scss']
})
export class LeftEntryComponent implements OnInit {

	@Input() livestory: Livestory;
  @Output() modalAsked = new EventEmitter<ActionCardData>();

  type:string;
  minuto:string;
  giocatore:string;

  typeLogoURL:string;

  nome_giocatore:string="";
  cognome_giocatore:string="";

  tiro_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-tiro.png";
  parata_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-parata.png";
  goal_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-goal.png"
  cronometro_img_url:string="https://www.livebomber.com/public/img/cronometro.png";
  flop_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-flop.png"

  display:boolean = true;  

  loaded:boolean = false;

  actionData:ActionCardData;

  constructor(private _api:APIService) { }

  getBoolForDisabled(s:string){
    if(s == "1"){
      return false;
    }else{
      return true;
    }
  }

  askModal() {
    if(!this.getBoolForDisabled(this.livestory.isvideo)){
      this.modalAsked.emit(this.actionData);
    }
  }

  getPlayer(id:string){
    return this._api.getPlayer(id).subscribe(g=>{
      //console.log(g);
      if(g){
        this.nome_giocatore = g.nome.toUpperCase();
        this.cognome_giocatore = g.cognome.toUpperCase();
        this.actionData.nome_g = g.nome.toUpperCase();
        this.actionData.cognome_g = g.cognome.toUpperCase();
        this.loaded = true;
      }else{
        this.display = false;
        this.loaded = true;
      }

      
    });
  }

  ngOnInit() {

    this.actionData = {
        url: this.livestory.id + "/"+ this.livestory.id_video,
        type: this.livestory.tag_name,
        nome_g:"",
        cognome_g:"",
        livestory:this.livestory.id
      }

  	this.type = this.livestory.tag_name;
  	this.minuto = this.livestory.minuto;
  	this.giocatore = this.livestory.giocatore;
    this.getPlayer(this.giocatore);
  	
  	switch (this.type) {
  		case "goal":
      this.typeLogoURL = this.goal_img_url;
      break;

      case "tiro":	
      this.typeLogoURL = this.tiro_img_url;
      break;

      case "parata":
      this.typeLogoURL = this.parata_img_url;
      break;

      case "flop":
      this.typeLogoURL = this.flop_img_url;
      break;

      default:
      break;
    }
  }

}
