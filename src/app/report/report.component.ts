import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, Validators, FormGroup,FormBuilder, ValidatorFn, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
	selector: 'app-report',
	templateUrl: './report.component.html',
	styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

	reportGroup: FormGroup;
	minPw = 18;
	confirmPassword;
	valid:boolean = false;

	constructor(
		private _ro:Router,
		private _loc:Location,
		private _formBuilder:FormBuilder) { }

	ngOnInit() {
		this.reportGroup = this._formBuilder.group({
			desc: ['', [Validators.required, Validators.minLength(this.minPw)]]
		});
	}

	get desc() { return this.reportGroup.get('desc'); }

	signUpUser(event){
		//this._ro.navigate(['terms'])
	}
	onDescInput() {
		if(this.reportGroup.valid){
			this.valid = true;
		}else{
			this.valid = false;
		}
	}
}