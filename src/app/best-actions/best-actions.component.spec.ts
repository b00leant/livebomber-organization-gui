import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestActionsComponent } from './best-actions.component';

describe('BestActionsComponent', () => {
  let component: BestActionsComponent;
  let fixture: ComponentFixture<BestActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
