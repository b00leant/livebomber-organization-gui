import { Component, OnInit } from '@angular/core';
import { Action } from '../models/action';
import { APIService } from '../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator'
import { Location } from '@angular/common';

@Component({
	selector: 'app-best-actions',
	templateUrl: './best-actions.component.html',
	styleUrls: ['./best-actions.component.scss']
})
export class BestActionsComponent implements OnInit {

	actions:Action[] = [];

	loaded:boolean = false;

	constructor(private _loc:Location, private _api:APIService,private _ar:ActivatedRoute) { }

	goBack(){
		this._loc.back();
	}

	updateProductsDisplayedInPage(e: PageEvent){

	}

	getBestActions(id:string,limit:number){
		return this._api.getBestActions(id,limit).subscribe(a=>{
			if(a.length > 0){
				for(let i = 0; i < a.length;i++){
					if(!a[i]['error']){
						let action:Action = a[i];
						this.actions.push(action);
					}
				}
			}
			this.loaded = true;
		});
	}
	

	ngOnInit() {
		let id = this._ar.snapshot.paramMap.get('id');
		this.getBestActions(id,12);
	}

}
