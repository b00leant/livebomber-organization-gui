import { Component, OnInit, Input,Output,EventEmitter,ViewChild,ElementRef} from '@angular/core';
import { Action } from '../../models/action';
import { APIService } from '../../services/api.service';
import { Player } from '../../models/player';
import { ActionCardData } from '../action-card-data';

@Component({
	selector: 'app-action-card',
	templateUrl: './action-card.component.html',
	styleUrls: ['./action-card.component.scss']
})
export class ActionCardComponent implements OnInit {
	@Input() action:Action;
	@Output() modalAsked = new EventEmitter<ActionCardData>();
	@ViewChild('header') private header:ElementRef;
	@Output() loadedEvent = new EventEmitter<boolean>();

	loaded:boolean = false;
	name_loaded:boolean = false;

	content_style = {
		height:'100%'
	}

	preview:string = "";
	player:Player;
	player_name:string = "";
	img_height:number = 0;
	date:Date;
	nome:string = "";
	cognome:string ="";

	askModal() {
		//let url_base = "https://www.livebomber.com/up/";
		let data:ActionCardData = {
			url: this.action.livestory + "/"+ this.action.id,
			type: this.action.tag,
			nome_g:this.player.nome,
			cognome_g:this.player.cognome,
			livestory:this.action.livestory,
			video_id:this.action.id,
			views:this.action.views,
			player_id:this.action.giocatore
		}
		this.modalAsked.emit(data);
	}

	constructor(private _api:APIService) { }

	getPlayer(){
		if(this.action.giocatore!="" && this.action.giocatore != "0"){
			return this._api.getPlayer(this.action.giocatore).subscribe(g=>{
				this.player = g;
				this.player_name = g.nome + " " + g.cognome;
				this.nome = g.nome;
				this.cognome = g.cognome;
				this.loaded = true;
				this.name_loaded = true;
			});
		}else{
			this.nome="-";
			this.cognome="-";
			this.name_loaded = true;
		}

	}

	getPreview(){
		let url_base = "https://www.livebomber.com/up/"
		this.preview = url_base + this.action.livestory + "/"+ this.action.id + "w.png";
	}

	typeLogoURL:string="";
	tiro_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-tiro.png";
	parata_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-parata.png";
	goal_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-goal.png"
	cronometro_img_url:string="https://www.livebomber.com/public/img/cronometro.png";
	flop_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-flop.png";
	magia_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-magia.png";
	the_wall_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-thewall.png"

	canestro_img_url:string="https://www.livebomber.com/assets/png/basket/basket-canestro.png";
	tripla_img_url:string="https://www.livebomber.com/assets/png/basket/basket-tripla.png"



	ngOnInit() {

		let informal_date = this.action.livestory;
		let day =informal_date.substring(0,2);
		let month = informal_date.substring(2,4);
		let year = informal_date.substring(4,8);
		let dateString = year+"-"+month+"-"+day+"T00:00:00";
		this.date = new Date(dateString);

		console.log(this.img_height);
		switch (this.action.tag) {
			case "goal":
			this.typeLogoURL = this.goal_img_url;
			break;

			case "tiro":  
			this.typeLogoURL = this.tiro_img_url;
			break;

			case "parata":
			this.typeLogoURL = this.parata_img_url;
			break;

			case "flop":
			this.typeLogoURL = this.flop_img_url;
			break;

			case "magia":
			this.typeLogoURL = this.magia_img_url;
			break;

			case "tripla":
			this.typeLogoURL = this.tripla_img_url;
			break;

			case "canestro":
			this.typeLogoURL = this.canestro_img_url;
			break;

			case "thewall":
			this.typeLogoURL = this.the_wall_img_url;
			break;

			default:
			break;
		}
		this.getPlayer();
		this.getPreview();
	}

	ngAfterViewInit(){
	}
}