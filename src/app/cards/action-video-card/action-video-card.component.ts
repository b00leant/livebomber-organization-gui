import { Component, OnInit, Input} from '@angular/core';
import { Action } from '../../models/action';
import { APIService } from '../../services/api.service';
import videojs from 'video.js';
import { ModalEventService } from '../../services/modal-event.service';
import { VideoModalData } from '../../video-modal/videoModalData'

@Component({
	selector: 'lb-action-video-card',
	templateUrl: './action-video-card.component.html',
	styleUrls: ['./action-video-card.component.scss']
})
export class ActionVideoCardComponent implements OnInit {

    data:VideoModalData;

    constructor(private _api:APIService,private _modalEventService:ModalEventService) {
    }

    share(){
        this.data = {
            type:this.action.tag,
            nome_g:"test",
            cognome_g:"test",
            url:this.action.url,
            livestory:this.action.livestory
        }
        this._modalEventService.launchShareEvent(this.data);
    }

    @Input() id:string;
    player_name = "";
    date:Date;
    type:string="";
    like:boolean = false;

    @Input() action:Action;

    typeLogoURL:string="";
    tiro_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-tiro.png";
    parata_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-parata.png";
    goal_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-goal.png"
    cronometro_img_url:string="https://www.livebomber.com/public/img/cronometro.png";
    flop_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-flop.png";
    magia_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-magia.png";
    the_wall_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-thewall.png"

    canestro_img_url:string="https://www.livebomber.com/assets/png/basket/basket-canestro.png";
    tripla_img_url:string="https://www.livebomber.com/assets/png/basket/basket-tripla.png"

    getPlayer(){
        if(this.action.giocatore!="" && this.action.giocatore != "0"){
            return this._api.getPlayer(this.action.giocatore).subscribe(g=>{
                this.player_name = g.nome + " " + g.cognome;
            });
        }else{
            this.player_name ="";
        }

    }
    ngOnInit() {
        this.getPlayer();
        let informal_date = this.action.livestory;
        let day =informal_date.substring(0,2);
        let month = informal_date.substring(2,4);
        let year = informal_date.substring(4,8);
        let dateString = year+"-"+month+"-"+day+"T00:00:00";
        this.date = new Date(dateString);
        this.type = this.action.tag;
    }

    initPlayer() {
        try {
            // setup the player via the unique element ID
            var element = document.getElementById('videoPlayer'+this.id);
            if (element == null) {
                throw "error loading blah";
            }
            // if we get here, all good!
            videojs(element, {}, () => { });
        }
        catch (e) {
        }
    }

    liked(){
        this.like = !this.like;
    }

    ngAfterViewInit() {
        switch (this.action.tag) {
            case "goal":
            this.typeLogoURL = this.goal_img_url;
            break;

            case "tiro":  
            this.typeLogoURL = this.tiro_img_url;
            break;

            case "parata":
            this.typeLogoURL = this.parata_img_url;
            break;

            case "flop":
            this.typeLogoURL = this.flop_img_url;
            break;

            case "magia":
            this.typeLogoURL = this.magia_img_url;
            break;

            case "tripla":
            this.typeLogoURL = this.tripla_img_url;
            break;

            case "canestro":
            this.typeLogoURL = this.canestro_img_url;
            break;

            case "thewall":
            this.typeLogoURL = this.the_wall_img_url;
            break;


            default:
            break;
        }
        this.initPlayer();
    }

}
