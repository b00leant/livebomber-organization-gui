import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionVideoCardComponent } from './action-video-card.component';

describe('ActionVideoCardComponent', () => {
  let component: ActionVideoCardComponent;
  let fixture: ComponentFixture<ActionVideoCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionVideoCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionVideoCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
