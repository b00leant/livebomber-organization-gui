import { Component, OnInit, Input } from '@angular/core';
import { Match } from '../../models/match';
@Component({
  selector: 'app-match-card',
  templateUrl: './match-card.component.html',
  styleUrls: ['./match-card.component.scss']
})
export class MatchCardComponent implements OnInit {
	@Input() match: Match;	
   date:Date;
  constructor() { }

  @Input() big:boolean = false;

  teamURL(id:string): string{
  	return "https://www.livebomber.com/up/uploads-sito/loghi/"+id+".png";
  }

  ngOnInit() {
    let informal_date = this.match.lsid;
    let day =informal_date.substring(0,2);
    let month = informal_date.substring(2,4);
    let year = informal_date.substring(4,8);
    let dateString = year+"-"+month+"-"+day+"T00:00:00";
    this.date = new Date(dateString);
  }

}
