import { Component, OnInit, Input} from '@angular/core';
import { Player } from '../../models/player';
import { APIService } from '../../services/api.service';

@Component({
	selector: 'app-player-card',
	templateUrl: './player-card.component.html',
	styleUrls: ['./player-card.component.scss']
})
export class PlayerCardComponent implements OnInit {
	@Input() player: Player;

	ranking="";
	loaded = true;
	custom_url:string="";
	default_url:string="https://league.livebomber.com/public/uploads/picGiocatori/player.jpg";
	constructor(private _api:APIService) { }
	ngOnInit() {
		this.getPlayer(this.player.id);
		this.custom_url = "https://league.livebomber.com/public/uploads/picGiocatori/"+this.player.id+".jpg";
	}

	getPlayer(id:string){
		return this._api.getPlayer(id).subscribe(p =>{
			//this.loaded = true;
			this.ranking=p.ranking;
		});
	}

}
