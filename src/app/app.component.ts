import { Component, Inject, ViewChild, Input, OnInit, ElementRef,HostListener } from '@angular/core';
import { Router,RouterOutlet,ActivatedRoute,NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import { HeaderComponent } from './header/header.component';
import { APIService } from './services/api.service';
import { SettingsService } from './services/settings.service';
import { Organization } from './models/organization';
import { Overlay } from '@angular/cdk/overlay';
import { FacebookLoginProvider , AuthService, SocialUser } from "angularx-social-login";
import { MatSidenav } from '@angular/material/sidenav';
import { slideInAnimation } from './animations';
import { LBAuthService } from './services/lbauth.service';
import { UserService } from './services/user.service';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { ModalEventService } from './services/modal-event.service';
import { VideoModalData } from './video-modal/videoModalData';
import { ShareService } from '@ngx-share/core';
import { MatSnackBar } from '@angular/material';
import { CopypasteService } from './services/copypaste.service';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { Angulartics2Facebook } from 'angulartics2/facebook';
import { Title } from '@angular/platform-browser';
import { SignUpFbResponse } from './models/fb-sign-up';

export interface SignInData {
  email: string;
  password: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [slideInAnimation]
})

export class AppComponent implements OnInit{

  @ViewChild(HeaderComponent)
  private headerComponent: HeaderComponent;

  @ViewChild('sidenav') sidenav: MatSidenav

  /* listener e funzione per rimuovere la searchbar quando si clicca fuori */

  /* questo per desktop */ 

  @HostListener('document:click', ['$event',])
  outClickHandler(event: MouseEvent): void {
    if(event.clientY >=50){  
      this.clearSearch();
    }
  }

  /* questo per mobile */ 

  onOverlayTap(event){
    this.clearSearch();
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }


  /* Menu button ripple settings */
  centered:boolean = true;
  disabled:boolean = false;
  unbounded:boolean = false;
  radius:number = 20;

  logged_in:boolean = false;
  user:SocialUser;

  mobile = false;
  overlay = false;

  pageTitle: string;
  currentURL: string = "";
  email: string;
  password: string;
  myOrganization: Organization;
  myOrganizationID:string = "";

  searching:boolean=false;
  value:string=""


  user_image:string="https://www.amicopolis.com/uploads/groups/photo/85199/300_square_e6be037025fb431bd2971c87292e813d.jpg";
  user_email:string="pippi@email.com";
  user_name:string="Pippi Calzelunghe";

  homeURL:string = "";
  categoriesURL:string = "";
  newsURL:string="";
  infoURL:string = "";

  fullscreen:boolean = false;

  searchOrgs:Organization[] = [];

  constructor(
    private title: Title,
    private angulartics2FacebookPixel: Angulartics2Facebook,
    private angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
    private snackBar: MatSnackBar,
    private _modalEventService: ModalEventService,
    private bottomSheet: MatBottomSheet,
    private _lbAuth:LBAuthService,
    private _user:UserService,
    private _copypaste:CopypasteService,
    private authService: AuthService,
    private _overlay:Overlay,
    private _ro: Router,
    private _ar: ActivatedRoute,
    public dialog: MatDialog,
    private api: APIService,
    private settings: SettingsService){

    angulartics2FacebookPixel.startTracking();
    angulartics2GoogleAnalytics.startTracking();

    _lbAuth.signIn$.subscribe(user=>{
      this.user_name = user.nome+" "+user.cognome
      this.user_email = user.email;
    })

    _lbAuth.signInWithFB$.subscribe(s=>{
      this.signInWithFB();
    });

    _copypaste.copy$.subscribe(c=>{
      this.copyMessage(c);
    });

    settings.organizationChosen$.subscribe(org =>{
      this.myOrganization = org;
      this.myOrganizationID = org.id;
      console.log("organizzazione scelta: "+org.id);
      this.buildLinksById(org.id);
    });

    _modalEventService.modalShareEvent$.subscribe(e=>{
      console.log(e);
      this.openBottomSheet(e);
    });

    _ro.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
        document.querySelector('.mat-sidenav-content').scrollTop = 0;
        title.setTitle(_ar.snapshot.firstChild.data['title']);

        this.pageTitle = _ar.snapshot.data['title'];
        this.currentURL = _ro.url;
        if(_ar.firstChild.snapshot.data['fullscreen']){
          this.fullscreen = true;
        }else{
          this.fullscreen = false;
        }
      }
    });

  }

  getPageTitle(){
    return this.title.getTitle();
  }

  buildLinksById(id:string){
    //Questa funzione serve a costruirele stringhe degli url in base all'organizzazione selezionata
    this.homeURL = "/channel/"+id+"/top";
    this.categoriesURL = "/channel/"+id+"/categories";
    this.infoURL = "/channel/"+id+"/info";
    this.newsURL = "/channel/"+id+"/news";
  }

  /* ~~~~~ FUNZIONI PER UI UX ~~~~~~ */

  isOrgView(){
    //Comunica se ci si trova in una sotto-route di channel
    if(this.currentURL.includes('channel')){
      return true;
    }else{
      return false;
    }
  }

  toggleOverlay(){
    //Toggle per Overay
    if(!this.overlay)
      this.overlay=true;
    else
      this.overlay=false;
  }

  newSearch(){
    //Mostra ricerca
    this.toggleOverlay();
    this.searching = true;
  }

  clearSearch(){
    //Pulisce ricerca
    this.value = "";
  }

  closeSearch(){
    //Esci dalla ricerca e la pulisce
    if(this.searching==true){
      this.toggleOverlay();
      this.value="";
      this.searching = false;
    }
  }

  openSignInDialog(): void {
    // VECCHIO MODAL PER LOGIN
    const dialogRef = this.dialog.open(SignInDialogComponent, {
      width: '250px',
      data: {email: this.email, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.email = result;
    });
  }

  openBottomSheet(e:VideoModalData): void {
    //Apre il BottomSheet
    const shareSheetRef = this.bottomSheet.open(ShareBottomSheet, {
      data: {
        url: e.url,
        type: e.type,
        nome_g:e.nome_g,
        cognome_g:e.cognome_g,
        livestory:e.livestory,
        shareURL:location.origin+"/video/"+e.video_id+"/"+e.player_id
      }
    });
  }

  /* ~~~~~ FUNZIONI AUTENTICAZIONE ~~~~~ */

  signInWithFB(){
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signOut(){
    // LogOut
    this.sidenav.toggle();
    this.myOrganization = null;
    this.myOrganizationID = "";
    if(this._user.logout()){
      this.authService.signOut();
      this._ro.navigate(['/'])
    }else{
      window.alert('Some problem occurred while exiting..');
    }
  }

  /* ~~~~~ FUNZIONI API ~~~~~ */

  getAllOrgs(){
    // Restituisce tutte le organizzazioni
    return this.api.getAllOrgs().subscribe(orgs=>{
      this.searchOrgs = orgs;
    });
  } 

  /* ~~~~~ FACILITIES ~~~~~ */

  clearOrganization(){
    // Cancella l'organizzazione corrente (non in localStorage)
    this.myOrganization = null;
    this.myOrganizationID = "";
  }

  copyMessage(val: string){
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.snackBar.open("copiato: "+val, "Ok", {
      duration: 2000,
    });
  }


  /* ~~~~~ FUNZIONI STANDARD ~~~~~ */

  ngOnInit() {

    this.getAllOrgs();
    this.authService.authState.subscribe((user) => {
      console.log("facebook auth state is changed..")
      if(user != null){
        this.api.createFacebookUser(user.firstName,user.lastName,user.email,user.id).subscribe(signed=>{
          let response:SignUpFbResponse = signed;
          console.log(signed);
          if(localStorage.getItem('lbac2k19')!=null){
          }else{
            if(signed.id!=""){
              console.log("Authentication: OK");
              localStorage.setItem('lblu2k19',signed.id);
              this._ro.navigate(['selection'])
            }
          }
        });
        this.user = user;
        this.logged_in = true;
        this.user_email = user.email;
        this.user_image = user.photoUrl;
        this.user_name = user.firstName+" "+user.lastName;
      }
    });
  }

  /* ~~~~~ EVENTI PER ANALYTICS ~~~~~ */

  sendEvent = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: 'eventCategory',
      eventLabel: 'eventLabel',
      eventAction: 'eventAction',
      eventValue: 10
    });
  }
}

@Component({
  selector: 'sign-in-dialog',
  templateUrl: 'sign-in-dialog.html',
})
export class SignInDialogComponent {

  constructor(
    private overlay: Overlay,
    public dialogRef: MatDialogRef<SignInDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SignInData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'share-bottom-sheet',
  templateUrl: './share-bottom-sheet.html'
})
export class ShareBottomSheet {

  url:string="";
  constructor(
    private _copypaste:CopypasteService,
    public share: ShareService,
    private _ro:Router,
    private bottomSheetRef: MatBottomSheetRef<ShareBottomSheet>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data:VideoModalData) {
    this.url = data.shareURL;
    console.log(this.url);
  }

  copy(s:string){
    this._copypaste.copy(s);
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }
}
