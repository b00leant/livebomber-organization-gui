import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Player } from '../models/player';
import { APIService } from '../services/api.service';

@Component({
	selector: 'app-profile-header',
	templateUrl: './profile-header.component.html',
	styleUrls: ['./profile-header.component.scss']
})
export class ProfileHeaderComponent implements OnInit {

	@Output() onBackClicked = new EventEmitter<void>();
	playerId:string;
	
	player:Player;

	loaded:boolean = false;

	nome:string = "";
	cognome:string= "";

	nvideo:string="";
	score:string = "";
	ranking:string ="";

	player_img:string="";
	default_url:string="https://league.livebomber.com/public/uploads/picGiocatori/player.jpg";

	constructor(private _api:APIService) { }
	
	backClick(){
		this.onBackClicked.emit();
	}

	getPlayerData(id:string){
		return this._api.getPlayer(id).subscribe(p =>{
			this.player = p;
			this.nome = p.nome;
			this.cognome = p.cognome;
			this.ranking = p.ranking;
			this.score = p.score;
			this.nvideo = "666";
			this.playerId =p.id;
			this.player_img = "https://league.livebomber.com/public/uploads/picGiocatori/"+p.id+".jpg";
			this.loaded = true;
		});
	}



	ngOnInit() {}

}
