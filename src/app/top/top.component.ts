import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Match } from '../models/match';
import { Team } from '../models/team';
import { Action } from '../models/action';
import { Person } from '../models/person';
import { Player } from '../models/player';
import { APIService } from  '../services/api.service';
import { VideoModalData } from '../video-modal/videoModalData';
import { VideoModalComponent } from '../video-modal/video-modal.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActionCardData } from '../cards/action-card-data';
import {
	DomSanitizer,
	SafeHtml,
	SafeUrl,
	SafeStyle
} from '@angular/platform-browser';

@Component({
	selector: 'app-top',
	templateUrl: './top.component.html',
	styleUrls: ['./top.component.scss']
})


export class TopComponent implements OnInit {

	thumbnail_url_player:string = "https://www.livebomber.com/up/030120192101464373/201303w.png";

	playerImg:any;


	matchesURL:string="";
	actionsURL:string="";

	/* variabili per il settaggio dei caroselli OWL-CAROUSEL */

	actionsOptions: any = {
		loop: true,
		autoplay: true,
		mouseDrag: true,
		pullDrag: true,
		touchDrag:true,
		dots: true,
		navSpeed: 700,
		responsive: {
			0: {
				items: 2
			},
			400: {
				items: 2
			},
			740: {
				items: 3
			},
			940: {
				items: 4
			}
		},
	};
	matchesOptions: any = {
		loop: true,
		autoplay: true,
		mouseDrag: true,
		pullDrag: true,
		touchDrag:true,
		dots: true,
		navSpeed: 700,
		responsive: {
			0: {
				items: 2
			},
			400: {
				items: 2
			},
			740: {
				items: 4
			},
			940: {
				items: 6
			}
		},
		
	};
	bombersOptions: any = {
		loop: true,
		autoplay: true,
		mouseDrag: true,
		pullDrag: true,
		touchDrag:true,
		dots: true,
		navSpeed: 700,
		responsive: {
			0: {
				items: 2
			},
			400: {
				items: 3
			},
			740: {
				items: 5
			},
			940: {
				items: 6
			}
		}
	}

	/*  variabili per il caricamento dinamico dei dati */

	loaded_m:boolean = false;
	loaded_a:boolean = false;
	loaded_b:boolean = false;
	rendered:boolean = false;


	openVideoModal(data:ActionCardData): void {
		//console.log("https://www.livebomber.com/up/181220182140274770/"+id_video+".mp4");
		const dialogRef = this.dialog.open(VideoModalComponent, {
			panelClass: 'lb-dialog-container',
			backdropClass: 'lb-backdrop-container',
			data: {
				url: "https://www.livebomber.com/up/"+data.url,
				type: data.type,
				nome_g:data.nome_g,
				cognome_g:data.cognome_g,
				livestory:data.livestory,
				video_id:data.video_id,
				views:data.views,
				player_id:data.player_id
			}
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	actionLoaded:boolean = false;
	actions: Action[] = [];

	
	g_team: Team = {
		id: "Liverpool",
		img_url: "https://www.livebomber.com/up/uploads-sito/loghi/4217.png",
		name:"azz",
		girone:"azz"
	};
	h_team: Team = {
		id: "Ibiza FC",
		img_url: "https://www.livebomber.com/up/uploads-sito/loghi/4217.png",
		name:"azz",
		girone:"azz"
	};

	
	bombers: Player[] = [
	/* fake bombers
	{name: "Francesco Totti", goal: 43, url_img: 'https://futhead.cursecdn.com/static/img/14/players/1238.png'},
	{name: "Gonzalo Higuain", goal: 63, url_img: 'https://futhead.cursecdn.com/static/img/14/players/167664.png'},
	{name: "Daniele De Rossi", goal: 22, url_img: 'https://futhead.cursecdn.com/static/img/19/players/53302.png'},
	{name: "Diego Armando Maradona", goal: 143, url_img: 'https://futhead.cursecdn.com/static/img/18/players_worldcup/190042.png'},
	{name: "Alessandro Del Piero", goal: 74, url_img: 'https://futhead.cursecdn.com/static/img/14/players/1075.png'},
	{name: "Radja Nainggolan", goal: 99, url_img: 'https://futhead.cursecdn.com/static/img/18/players_worldcup/178518.png'}*/
	];



	matches: Match[] = [
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"},
	{sq1:"Chelsea",sq2:"Racing Santander",ris1:"10",ris2:"7",sq1id:"5050",sq2id:"4722",lsid:"280120192002225050"},
	{sq1:"San Paolo",sq2:"Vaco Da Gama",ris1:"9",ris2:"5",sq1id:"4843",sq2id:"4844",lsid:"280120192006394843"}
	];
	constructor(private  apiService:  APIService,private _ar: ActivatedRoute,private sanitization:DomSanitizer,public dialog: MatDialog) {
		
	}

	ngOnInit() {
		this.getBestActions();
		this.getBombers();
		this.playerImg	 = this.sanitization.bypassSecurityTrustStyle(`url(${this.thumbnail_url_player})`);
		this.getURLS();
	}

	getURLS(){
		let id = this._ar.parent.snapshot.paramMap.get('id');
		this.actionsURL="/best-actions/"+id;
		this.matchesURL="/best-matches/"+id;
	}

	getBombers(){
		let organizationID = this._ar.parent.snapshot.paramMap.get('id');
		return this.apiService.getBestPlayers(organizationID,24).subscribe(b=>{
			//console.log(b);
			this.bombers = b;
			this.loaded_b = true;
		});
		
	}

	getBestActions(){
		let organizationID = this._ar.parent.snapshot.paramMap.get('id');
		return this.apiService.getBestActions(organizationID,12).subscribe(a => {
			//console.log(a);
			if(a.length > 0){
				for(let i = 0; i < a.length;i++){
					if(!a[i]['error']){
						let action:Action = a[i];
						this.actions.push(action);
					}
				}
			}
			this.loaded_a = true;
			this.loaded_m = true;
		});
	}

	preventScroll(){

	}

	ngAfterViewInit(){
	}

}
