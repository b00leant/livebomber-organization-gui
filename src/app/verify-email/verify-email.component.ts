import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {FormControl, Validators, FormGroup,FormBuilder, ValidatorFn, ValidationErrors} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { LBAuthService } from '../services/lbauth.service';
import { CookieService } from 'ngx-cookie-service';
import { APIService } from '../services/api.service';


@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
  constructor(
    private _ar:ActivatedRoute,
    private cookieService: CookieService,
    private router:Router,
    private auth:LBAuthService,
    private _loc:Location,
    private _api:APIService,
    private _formBuilder:FormBuilder) { }

  error:boolean=false;
  loaded:boolean=false;
  hash:string = "";
  uid:string = "";
  email:string = "";

  ngOnInit() {

    this._ar.queryParams.subscribe(params=>{
      this.uid = params['id']
      this.hash = params['hash']
      this.email = params['email']
      if(this.email !="" && this.hash !=""){
        this._api.verifyEmail(this.email,this.hash).subscribe(res=>{
          this.loaded=true;
          if(res.result){
            this.router.navigate(['login'])
          }else{
            this.router.navigate(['welcome'])
          }
        })
      }else{
        this.error=true;
      }
    });

  }
}
