import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FoldingBannerComponent } from './folding-banner.component';

describe('FoldingBannerComponent', () => {
  let component: FoldingBannerComponent;
  let fixture: ComponentFixture<FoldingBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FoldingBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FoldingBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
