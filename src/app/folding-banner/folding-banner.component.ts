import { Component, OnInit,Input } from '@angular/core';

@Component({
	selector: 'app-folding-banner',
	templateUrl: './folding-banner.component.html',
	styleUrls: ['./folding-banner.component.scss']
})
export class FoldingBannerComponent implements OnInit {

	@Input() ranking;
	constructor() { }

	ngOnInit() {
	}

}
