import { Component, OnInit, Inject,ViewChild,AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VideoModalData } from './videoModalData';
import videojs from 'video.js';
import { ModalEventService } from '../services/modal-event.service';
import { Router } from '@angular/router';
import { APIService } from '../services/api.service'

@Component({
	selector: 'app-video-modal',
	templateUrl: './video-modal.component.html',
	styleUrls: ['./video-modal.component.scss']
})
export class VideoModalComponent implements OnInit {

    like:boolean = false;

    type:string = "";

    video_url:string = "";

    poster_url:string = "";

    options = {
    	autoplay: true
    }


    video_id:string="";
    player_name:string = "";
    views:string="";
    date:Date;

    typeLogoURL:string="";
    tiro_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-tiro.png";
    parata_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-parata.png";
    goal_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-goal.png"
    cronometro_img_url:string="https://www.livebomber.com/public/img/cronometro.png";
    flop_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-flop.png";
    magia_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-magia.png";
    the_wall_img_url:string="https://www.livebomber.com/assets/png/calcio/calcio-thewall.png"

    canestro_img_url:string="https://www.livebomber.com/assets/png/basket/basket-canestro.png";
    tripla_img_url:string="https://www.livebomber.com/assets/png/basket/basket-tripla.png"

    
    constructor(
        private _api:APIService,
        private _ro:Router,
        private _modalEventService: ModalEventService,
        public dialogRef: MatDialogRef<VideoModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: VideoModalData) {
        this.video_id = data.video_id
        this.video_url = data.url+".mp4";
        this.poster_url = data.url+"w.png";
        this.type = data.type;
        this.player_name = (data.nome_g+" "+data.cognome_g).toUpperCase();
        this.views = data.views;
        let informal_date = data.livestory;
        let day =informal_date.substring(0,2);
        let month = informal_date.substring(2,4);
        let year = informal_date.substring(4,8);
        let dateString = year+"-"+month+"-"+day+"T00:00:00";
        this.date = new Date(dateString);
    }

    reportVideo(){
        this.dialogRef.close();
        this._ro.navigate(['report'])
    }

    onNoClick(): void {
    	this.dialogRef.close();
    }

    ngOnInit() {
    }

    initPlayer() {
        try {
            // setup the player via the unique element ID
            var element = document.getElementById('videoPlayer');
            if (element == null) {
                throw "error loading blah";
            }
            // if we get here, all good!
            videojs(element, {}, () => { });
        }
        catch (e) {
        }
    }

    liked(){
        //let user_id = this._api.getus to overwrite the static '424'
        if(!this.like){
            console.log(this.video_id);
            this._api.setActivity('like','video',this.video_id,'424').subscribe(res=>{
                console.log(res);
            })
        }
        this.like = !this.like
    }

    share(){
        this._modalEventService.launchShareEvent(this.data);
    }

    ngAfterViewInit() {
        switch (this.type) {
            case "goal":
            this.typeLogoURL = this.goal_img_url;
            break;

            case "tiro":  
            this.typeLogoURL = this.tiro_img_url;
            break;

            case "parata":
            this.typeLogoURL = this.parata_img_url;
            break;

            case "flop":
            this.typeLogoURL = this.flop_img_url;
            break;

            case "magia":
            this.typeLogoURL = this.magia_img_url;
            break;

            case "tripla":
            this.typeLogoURL = this.tripla_img_url;
            break;

            case "canestro":
            this.typeLogoURL = this.canestro_img_url;
            break;

            case "thewall":
            this.typeLogoURL = this.the_wall_img_url;
            break;


            default:
            break;
        }
        this.initPlayer();
    }

}
