export interface VideoModalData {
	video_id?:string;
	type?:string;
	nome_g?:string;
	cognome_g?:string;
	url: string;
	livestory:string;
	shareURL?:string;
	views?:string;
	player_id?:string;
}