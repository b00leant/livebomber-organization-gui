import { Directive, ElementRef, Output, Input, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[clickOutside]'
})
export class ClickOutsideDirective {

  @Input() public visible:string;
  @Output() public clickOutside = new EventEmitter();
  constructor(private _elementRef : ElementRef) { }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const isClickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!isClickedInside) {
      console.log("touched?");
      if(this.visible == "true"){
        this.clickOutside.emit(null);
      }else{
        this.visible = "true";
      }
    }
  }
  @HostListener('touchend', ['$event.target'])
  public onTouch(targetElement) {
    const isClickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (!isClickedInside) {
      console.log("this have been touched");
      if(this.visible == "true"){
        this.clickOutside.emit(null);
      }else{
        this.visible = "true";
      }
    }
  }
}