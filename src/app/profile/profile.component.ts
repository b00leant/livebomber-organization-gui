import { Component, OnInit, ViewChild } from '@angular/core';
import { Player } from '../models/player';
import { ProfileHeaderComponent } from '../profile-header/profile-header.component';
import { Location } from '@angular/common';
import { Action } from '../models/action';
import { APIService } from '../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ActionCardData } from '../cards/action-card-data';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { VideoModalComponent } from '../video-modal/video-modal.component';
import { VideoModalData } from '../video-modal/videoModalData';


@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

	@ViewChild(ProfileHeaderComponent) private header:ProfileHeaderComponent;
	player:Player;

	grid:boolean = true;

	action1:Action = {
		giocatore: "36349",
		id: "183161",
		likes: "1",
		livestory: "161120181933504672",
		organizzazione: "13",
		preferito: "1",
		score: "221",
		tag: "goal",
		tag_title: "goal",
		thumb: "https://s3.livebomber.com/up/161120181933504672/183161w.png",
		url: "https://s3.livebomber.com/up/161120181933504672/183161.mp4",
		views: "61",
		views_u: "36"
	}

	action2:Action = {
		giocatore: "36349",
		id: "183161",
		likes: "1",
		livestory: "161120181933504672",
		organizzazione: "13",
		preferito: "1",
		score: "221",
		tag: "goal",
		tag_title: "goal",
		thumb: "https://s3.livebomber.com/up/161120181933504672/183161w.png",
		url: "https://s3.livebomber.com/up/161120181933504672/183161.mp4",
		views: "61",
		views_u: "36"
	}

	loaded:boolean =false;

	constructor(private _api:APIService,private _location:Location,private _ro:Router,private _ar:ActivatedRoute,public dialog: MatDialog) { }

	ngOnInit() {
		console.log(this._ar.snapshot.paramMap.get('id'));
		let id = this._ar.snapshot.paramMap.get('id');
		this.getPlayer(id);
	}

	goBack(){
		this._location.back();
	}
	getPlayer(id:string){
		return this._api.getPlayer(id).subscribe(p =>{
			this.player = p;
			this.header.getPlayerData(p.id);
			this.loaded = true;
		});
	}

	openVideoModal(action:Action): void {
		//console.log("https://www.livebomber.com/up/181220182140274770/"+id_video+".mp4");
		const dialogRef = this.dialog.open(VideoModalComponent, {
			panelClass: 'lb-dialog-container',
			backdropClass: 'lb-backdrop-container',
			data: {
				url: "https://s3.livebomber.com/up/"+action.url,
				type: action.tag,
				nome_g:"test",
				cognome_g:"test",
				livestory:action.livestory
			}
		});

		dialogRef.afterClosed().subscribe(result => {
		});
	}

	actions: Action[] = [
	this.action1,this.action2,this.action1,this.action2,this.action1,this.action2,this.action1,this.action1,
	this.action2,this.action1,this.action2
	/*
	{type: "p",thumbnail_url:"https://www.livebomber.com/up/030120192101464373/201303w.png",player:"Marco Rossi",player_img:"https://futhead.cursecdn.com/static/img/14/players/1238.png"},
	{type: "g",thumbnail_url:"https://www.livebomber.com/up/030120192101464373/201303w.png",player:"Pippo Franco",player_img:"https://futhead.cursecdn.com/static/img/14/players/1238.png"},
	{type: "p",thumbnail_url:"https://www.livebomber.com/up/030120192101464373/201303w.png",player:"Gino Guidi",player_img:"https://futhead.cursecdn.com/static/img/14/players/1238.png"},
	{type: "p",thumbnail_url:"https://www.livebomber.com/up/030120192101464373/201303w.png",player:"Gino Guidi",player_img:"https://futhead.cursecdn.com/static/img/14/players/1238.png"}
	*/];
}
