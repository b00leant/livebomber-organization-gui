import { BrowserModule, HammerGestureConfig, BrowserTransferStateModule, HammerLoader, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { OverlayModule } from '@angular/cdk/overlay';
import { AgmCoreModule } from '@agm/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';

//importo material.module che contiene tutti i moduli per il material design di Angular Material
import { MaterialModule } from './material.module';
import { MatExpansionModule } from '@angular/material/expansion';

//import plugins
import { CarouselModule } from 'ngx-owl-carousel-o';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { ShareButtonModule } from '@ngx-share/button';
import { SocialLoginModule, AuthServiceConfig } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from "angularx-social-login";
import { ContentLoaderModule } from '@netbasal/ngx-content-loader';
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { Angulartics2Facebook } from 'angulartics2/facebook';
import { CookieService } from 'ngx-cookie-service';

//import pipes
import { FilterPipe, OrgFilter, PlayersFilter } from './pipes/filter.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';

//import directives
import { ClickOutsideDirective }  from './directives/clickOutside.directive';
import { ImagePreloadDirective } from './directives/image-preload.directive';
import { LoadingImageDirective } from './directives/loading-image.directive';

//import interceptors
import { APIInterceptor } from './apiInterceptror';

//import components
import { AppComponent, SignInDialogComponent,ShareBottomSheet } from './app.component';
import { TopComponent } from './top/top.component';
import { CategoriesComponent } from './categories/categories.component';
import { LeagueComponent } from './league/league.component';
import { InfoComponent } from './info/info.component';
import { PlayerComponent } from './player/player.component';
import { HeaderComponent } from './header/header.component';
import { SelectionComponent } from './selection/selection.component';
import { MainComponent } from './main/main.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileHeaderComponent } from './profile-header/profile-header.component';
import { ProgressComponent } from './progress/progress.component';
import { ActionCardComponent } from './cards/action-card/action-card.component';
import { MatchCardComponent } from './cards/match-card/match-card.component';
import { PlayerCardComponent } from './cards/player-card/player-card.component';
import { VideoModalComponent } from './video-modal/video-modal.component';
import { TimelineListComponent } from './timeline-list/timeline-list.component';
import { RightEntryComponent } from './timeline-list/right-entry/right-entry.component';
import { LeftEntryComponent } from './timeline-list/left-entry/left-entry.component';
import { LineComponent } from './timeline-list/line/line.component';
import { TimelineStartEntryComponent } from './timeline-list/timeline-start-entry/timeline-start-entry.component';
import { AboutComponent } from './about/about.component';
import { SupportComponent } from './support/support.component';
import { NewsComponent } from './news/news.component';
import { BestActionsComponent } from './best-actions/best-actions.component';
import { SplashScreenComponent } from './splash-screen/splash-screen.component';
import { SplashButtonsComponent } from './splash-buttons/splash-buttons.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { BestMatchesComponent } from './best-matches/best-matches.component';
import { FoldingBannerComponent } from './folding-banner/folding-banner.component';
import { SettingsComponent } from './settings/settings.component';
import { HorizontalScrollerComponent } from './horizontal-scroller/horizontal-scroller.component';
import { ActionVideoCardComponent } from './cards/action-video-card/action-video-card.component';
import { SingleVideoPageComponent } from './single-video-page/single-video-page.component';
import { ToAppComponent } from './to-app/to-app.component';

// AUTH GUARD
import { AuthGuard } from './auth.guard';

// RESOLVER
import { SingleVideoResolver } from "./resolvers/video.resolver";
import { PlayerResolver } from "./resolvers/player.resolver";

// SERVICES 
import { APIService } from './services/api.service';
import { LBAuthService } from './services/lbauth.service';
import { UserService } from './services/user.service';

// ICONS 
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faRedditAlien } from '@fortawesome/free-brands-svg-icons/faRedditAlien';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons/faLinkedinIn';
import { faGooglePlusG } from '@fortawesome/free-brands-svg-icons/faGooglePlusG';
import { faTumblr } from '@fortawesome/free-brands-svg-icons/faTumblr';
import { faPinterestP } from '@fortawesome/free-brands-svg-icons/faPinterestP';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { faVk } from '@fortawesome/free-brands-svg-icons/faVk';
import { faFacebookMessenger } from '@fortawesome/free-brands-svg-icons/faFacebookMessenger';
import { faTelegramPlane } from '@fortawesome/free-brands-svg-icons/faTelegramPlane';
import { faMix } from '@fortawesome/free-brands-svg-icons/faMix';
import { faXing } from '@fortawesome/free-brands-svg-icons/faXing';
import { faLine } from '@fortawesome/free-brands-svg-icons/faLine';
import { faCommentAlt } from '@fortawesome/free-solid-svg-icons/faCommentAlt';
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus';
import { faCopy } from '@fortawesome/free-solid-svg-icons/faCopy';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons/faEllipsisH';
import { faLink } from '@fortawesome/free-solid-svg-icons/faLink';
import { faExclamation } from '@fortawesome/free-solid-svg-icons/faExclamation';
import { faPrint } from '@fortawesome/free-solid-svg-icons/faPrint';
import { faCheck } from '@fortawesome/free-solid-svg-icons/faCheck';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faStopwatch, faFutbol, faCog } from '@fortawesome/free-solid-svg-icons';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TermsComponent } from './terms/terms.component';
import { ReportComponent } from './report/report.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { WaitAEmailComponent } from './wait-a-email/wait-a-email.component';

//imposto la configurazione di gestures touch per HammerJS

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any> {
    'pinch': { enable: false },
    'rotate': { enable: false }
  }
}

export function provideConfig() {
  return config;
}

//inizializzo il provider di autorizzazione OAUTH con l'app id di Livebomber

let config = new AuthServiceConfig([
{
  id: FacebookLoginProvider.PROVIDER_ID,
  provider: new FacebookLoginProvider("222541318130134")
}
]);

//aggiungo alla libreria di icone le varie web-icons in svg

library.add(faStopwatch, faFutbol, faCog, faCopy,
  faFacebookF, faTwitter, faLinkedinIn, faGooglePlusG, faPinterestP, faRedditAlien, faTumblr,
  faWhatsapp, faVk, faFacebookMessenger, faTelegramPlane, faMix, faXing, faCommentAlt, faLine,
  faEnvelope, faCheck, faPrint, faExclamation, faLink, faEllipsisH, faMinus);


//definisco le routes
const appRoutes: Routes = [
{
  path: 'channel/:id',
  canActivate: [AuthGuard],            //<---- parent component declared here
  component: MainComponent,
  data: { title: 'Home'},
  children: [                         //<---- child components declared here
  {
    path: 'top',
    component: TopComponent,
    data: { title: 'Home'}
  },
  {
    path: 'categories',
    component: CategoriesComponent,
    data: { title: 'Categorie'}
  },
  {
    path: 'categories/league/:id',
    component: LeagueComponent,
    data: { title: 'Girone'}
  },
  {
    path: 'info',
    component: InfoComponent,
    data: { title: 'Info'}
  },
  {
    path: 'news',
    component: NewsComponent,
    data: { title: 'News'}
  }
  ]
},
{
  path:'toapp',
  component: ToAppComponent
},
{
  path:'verifyemail',
  component: VerifyEmailComponent,
  data: {
    title: 'Verifica Email',
    fullscreen:true
  }
},
{
  path:'video/:id/:player_id',
  component:SingleVideoPageComponent,
  resolve: {
    action: SingleVideoResolver,
    player: PlayerResolver
  }
},
{
  path:'waitamail',
  component:WaitAEmailComponent,
  data:{
    title:'Email di verifica invitata',
    fullscreen:true
  }
},
{
  path: 'settings',
  component: SettingsComponent,
  canActivate: [AuthGuard],
  data: { title: 'Impostazioni'}
},
{
  path: 'best-matches/:id',
  component: BestMatchesComponent,
  data: { title: 'Migliori Partite'}
},
{
  path: 'best-actions/:id',
  component: BestActionsComponent,
  data: { title: 'Migliori Azioni'}
},
{
  path:'',
  redirectTo: '/welcome',
  pathMatch: 'full'
},
{
  path: 'welcome',
  canActivate: [AuthGuard],
  component: SplashScreenComponent,
  data: { 
    title: 'Welcome',
    fullscreen:true,
    //animation: 'SplashPage'
  },
},
{
  path: 'login',
  canActivate: [AuthGuard],
  component: SignInComponent,
  data: {
    title: 'Accedi',
    fullscreen:true
    //animation: 'LoginPage'
  }
},
{
  path: 'signup',
  canActivate: [AuthGuard],
  component: SignUpComponent,
  data: {
    title: 'signup', fullscreen:true,
    //animation: 'SignupPage'
  }
},
{
  path: 'profile/:id',
  component: ProfileComponent,
  canActivate: [AuthGuard],
  data: { title: 'Profilo'}
},
{
  path: 'selection',
  component: SelectionComponent,
  canActivate: [AuthGuard],
  data: { title: 'Selezione',fullscreen:true}
},
{
  path: 'match/:id',
  component: TimelineListComponent,
  canActivate: [AuthGuard],
  data: { title: 'Match', animation:'Match'}
},
{
  path: 'about',
  canActivate: [AuthGuard],
  component: AboutComponent,
  data: { title: 'About'}
},
{
  path: 'support',
  canActivate: [AuthGuard],
  component: SupportComponent,
  data: { title: 'Support'}
},
{
  path: "terms",
  canActivate: [AuthGuard],
  component:TermsComponent,
  data: { title: "Termini e condizioni"}
},
{
  path: "report",
  component:ReportComponent,
  canActivate: [AuthGuard],
  data: { title: "Segnalazione Video"}
},
{ path: '**', component: PageNotFoundComponent }


];

@NgModule({
  declarations: [
  AppComponent,
  TopComponent,
  CategoriesComponent,
  LeagueComponent,
  InfoComponent,
  PlayerComponent,
  SignInDialogComponent,
  ShareBottomSheet,
  HeaderComponent,
  SelectionComponent,
  MainComponent,
  ProfileComponent,
  ProfileHeaderComponent,
  ProgressComponent,
  ActionCardComponent,
  MatchCardComponent,
  PlayerCardComponent,
  VideoModalComponent,
  TimelineListComponent,
  RightEntryComponent,
  LeftEntryComponent,
  LineComponent,
  FilterPipe,
  OrgFilter,
  PlayersFilter,
  TimelineStartEntryComponent,
  AboutComponent,
  SupportComponent,
  ClickOutsideDirective,
  NewsComponent,
  ImagePreloadDirective,
  TruncatePipe,
  BestActionsComponent,
  SplashScreenComponent,
  SplashButtonsComponent,
  SignUpComponent,
  SignInComponent,
  BestMatchesComponent,
  FoldingBannerComponent,
  SettingsComponent,
  LoadingImageDirective,
  HorizontalScrollerComponent,
  ActionVideoCardComponent,
  SingleVideoPageComponent,
  ToAppComponent,
  PageNotFoundComponent,
  TermsComponent,
  ReportComponent,
  VerifyEmailComponent,
  WaitAEmailComponent,
  ],
  imports: [

  // Add .withServerTransition() to support Universal rendering.
  // The application ID can be any identifier which is unique on
  // the page.
  BrowserModule.withServerTransition({appId: 'livebomber-ng'}),
  BrowserTransferStateModule,
  ContentLoaderModule,
  SocialLoginModule,
  BrowserModule,
  BrowserAnimationsModule,
  RouterModule.forRoot(appRoutes,
  {
    scrollPositionRestoration: 'enabled',
    enableTracing: false 
  }),
  Angulartics2Module.forRoot(),
  AgmCoreModule.forRoot(
  {
    apiKey: 'AIzaSyCng0QoIY4tra2irzFrHLoFU7rpE0XF7FQ'
  }),
  HttpClientModule,
  LoadingBarRouterModule,
  BrowserAnimationsModule,
  MaterialModule,
  FlexLayoutModule,
  CarouselModule,
  MatExpansionModule,
  FormsModule,
  ReactiveFormsModule,
  BrowserModule,
  ScrollDispatchModule,
  FontAwesomeModule,
  ShareButtonModule
  ],
  providers: [
  CookieService,
  LBAuthService,
  UserService,
  AuthGuard,
  APIService,
  SingleVideoResolver,
  PlayerResolver,
  {
    provide: AuthServiceConfig,
    useFactory: provideConfig
  },
  {
    provide: HAMMER_GESTURE_CONFIG,
    useClass: MyHammerConfig
  }
    /*{
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    }*/
    ],
    bootstrap: [AppComponent],
    entryComponents: [
    ShareBottomSheet,
    SignInDialogComponent,
    VideoModalComponent
    ]
  })
export class AppModule {
}
