import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { APIService } from '../services/api.service';
import { SettingsService } from '../services/settings.service';
import { Organization } from '../models/organization';
import { Router,RouterOutlet,ActivatedRoute,NavigationEnd } from '@angular/router';
import { HeaderComponent } from '../header/header.component';

@Component({
	selector: 'app-main',
	templateUrl: './main.component.html',
	styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

	@ViewChild(HeaderComponent)
	private headerComponent: HeaderComponent;

	// variabili che contengono il titolo della pagina e del sottogruppo selezionato
	pageTitle:string;
	leagueTitle:string;

	// variabile che contiene le informazioni dell'organizzazione scelta e modificata tramite servizio
	org: Organization;

	// contiene il nome dell'organizzazione da mostrare
	orgTitle:string = "";

	// variabili di routing

	categoriesURL:string ="";
	homeURL = "";
	infoURL = "";

	// url corrente
	currentURL:string;

	// se settata a true mi consente di tornare indietro dalla vista sottogruppi
	backNav = "";

	constructor(
		private _ro: Router,
		private _ar: ActivatedRoute,
		private api: APIService,
		private settings: SettingsService){

		settings.organizationChosen$.subscribe(org => {
			this.org = org;
		});

		_ro.events.subscribe(event => {
			// catturo l'evento di fine navigazione 
			if (event instanceof NavigationEnd) { 
				this.currentURL = _ro.url;
				/* se l'url contiene la stringa "league" allora siamo in gestione
				 sotto gruppi e devo poter tornare indietro, quindi mostro la vista
				 gestita dalla variabile booleana "backNav"
				*/
				if (this.currentURL.includes('league')) {
					this.backNav = "enabled";
					console.log(this._ar.firstChild.snapshot.queryParamMap.get('title'));
					this.pageTitle = this._ar.firstChild.snapshot.queryParamMap.get('title');
				}else{
					this.backNav = "disabled";
					this.pageTitle = _ar.firstChild.snapshot.data['title'];
				}
			}
		});
	}

	ngOnInit() {
		// prendo l'id dall'url e lo imposto come id organizzazione
		let organizationID = this._ar.snapshot.paramMap.get('id');

		// se l'id è vuoto faccio redirect verso la vista di selezione
		if(organizationID == "" || organizationID == null){
			this._ro.navigate(['/selection']);
		}else{
			/* se non è vuoto allora chiamo la funzione "getOrg" della
			variabile headerComponent di tipo HeaderComponent che prende
			l'id di tipo stringa e setta titolo e immagine
			*/
			this.headerComponent.getOrg(organizationID);
			this.api.getOrg(organizationID).subscribe(org=>{
				this.settings.setOrganization(org[0]);
			});

			// setto le variabili di routing
			this.categoriesURL = "/channel/"+organizationID+"/categories";
			this.homeURL = "/channel/"+organizationID+"/top";
			this.infoURL = "/channel/"+organizationID+"/info";
		}
	}

}
