import { Injectable } from '@angular/core';
import { HttpClient,HttpErrorResponse, HttpHeaders,HttpParams } from '@angular/common/http';
import { Organization } from '../models/organization';
import { Group } from '../models/group';
import { LeagueCalendar } from '../models/leagueCalendar';
import { LeagueDay } from '../models/leagueDay';
import { Classification } from '../models/classification';
import { Livestory } from '../models/livestory';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Team } from '../models/team';
import { Action } from '../models/action';
import { Player } from '../models/player';
import { AuthToken } from '../models/auth-token';
import { TokenValidation } from '../models/token-validation';
import { SignUpFbResponse } from '../models/fb-sign-up';
import { ActivityResponse } from '../models/activity-response'
import { SignUpResponse } from '../models/sign-up-response'
import { UserInfo } from '../models/user-info';
import { ValidationResponse } from '../models/validation-response'

interface ServerData {
	records: Organization[];
}

const httpOptions = {
	headers: new HttpHeaders({
		'Content-Type':'application/x-www-form-urlencoded',
		'Authorization': 'my-auth-token',
		'Access-Control-Allow-Headers':'content-type',
		'Access-Control-Allow-Origin':'*'
	})
};

const httpOptionsJson = {
	headers: new HttpHeaders({
		'Content-Type':  'application/json',
		//'Authorization': 'my-auth-token',
		//'Access-Control-Allow-Headers':'content-type',
		//'Access-Control-Allow-Origin':'*'
	})
};


@Injectable({
	providedIn: 'root'
})
export class APIService {

	API_URL  =  'https://api.livebomber.com/v1/resources';

	constructor(private httpClient: HttpClient) {}
	/*
	Authentication functions
	*/

	getUser(uid:string):Observable<UserInfo>{
		return this.httpClient.get(this.API_URL+'/utenti/'+uid).pipe(map(res => res['records'][0]));
	}

	setActivity(type:string,obj:string,objId:string,subjId:string): Observable<ActivityResponse>{
		return this.httpClient.post<any>(this.API_URL+'/activities/'+type+'/'+obj+'/'+objId+'/'+subjId,httpOptions)
	}

	getAllOrgs(): Observable<Organization[]>{
		return  this.httpClient.get(this.API_URL+'/organizzazioni/all').pipe(map(res => res['records']));
	}
	getOrg(id): Observable<Organization[]>{
		return this.httpClient.get(this.API_URL+'/organizzazioni/'+id).pipe(map(res => res['records']));
	}

	getOrgGroups(id:string): Observable<Group[]>{
		return this.httpClient.get(this.API_URL+'/organizzazioni/'+id+'/gruppi').pipe(map(res => res['records']));
	}

	getOrgName(id:string):	 Observable<Organization[]>{
		return this.httpClient.get(this.API_URL+'/organizzazioni/'+id).pipe(map(res => res['records']));
	}

	getLeagueCalendar(id:string): Observable<LeagueDay[]>{
		return this.httpClient.get(this.API_URL+'/gruppi/'+id+'/calendari').pipe(map(res=>res['records'][0]['giornata']));
	}

	getLeagueClassification(id:string): Observable<Classification[]>{
		return this.httpClient.get(this.API_URL+'/gruppi/'+id+'/classifiche').pipe(map(res=>res['records']));
	}

	getLeagueTeams(id:string): Observable<Team[]>{
		return this.httpClient.get(this.API_URL+'/gruppi/'+id+'/squadre').pipe(map(res=>res['records']));
	}

	getLivestory(id:string): Observable<Livestory[]>{
		return this.httpClient.get(this.API_URL+'/livestories/'+id).pipe(map(res => res['records']));
	}

	getActionForResolver(id:string):Observable<Action>{
		return this.httpClient.get(this.API_URL+'/videos/'+id).pipe(map(res => res['records']));
	}
	/*

	getAction(id:string):Observable<Action>{
		return this.httpClient.get('https://api.livebomber.com/v1/resources/videos/'+id).pipe(map(res => res['records'][0]));
	}

	*/
	getAction(id:string):Observable<Action>{
		return this.httpClient.get(this.API_URL+'/videos/'+id).pipe(map(res => res['records'][0]));
	}

	getBestActions(id:string,limit:number):Observable<Action[]>{
		return this.httpClient.get(this.API_URL+'/organizzazioni/'+id+'/videos&limit='+limit.toString()+'&preferiti')
		.pipe(
			map((res) => {
				console.log(res['records']);
				let empty:Action[] = [];
				if(res['records']){
					return res['records']
				}else{
					return empty;
				}
			})
			);	
	}

	getBestPlayers(id:string,limit:number):Observable<Player[]>{
		return this.httpClient.get(this.API_URL+'/organizzazioni/'+id+'/giocatori&limit='+limit.toString()+'&preferiti').pipe(map(res => res['records']));
	}

	getOrgPlayers(id:string,limit:number):Observable<Player[]>{
		return this.httpClient.get(this.API_URL+'/organizzazioni/'+id+'/giocatori&limit='+limit.toString()).pipe(map(res=>res['records']));
	}

	getPlayer(id:string):Observable<Player>{
		if(id!="" && id)
			return this.httpClient.get(this.API_URL+'/giocatori/'+id).pipe(map(res=>res['records'][0]));
	}

	getPlayerForResolver(id:string):Observable<Player>{
		return this.httpClient.get(this.API_URL+'/giocatori/'+id).pipe(map(res => res['records']));
	}


	/*
	Authentication functions
	*/

	sendEmail(id:string):Observable<any>{
		let key = "208djJDK38339!skalx1"
		return this.httpClient.post<any>(this.API_URL+'/utenti/'+id+'/convalidaemail',
			{key:key},httpOptionsJson)
	}

	verifyEmail(email:string,hash:string):Observable<ActivityResponse>{
		return this.httpClient.post<any>(this.API_URL+'/utenti/validazioneaccount',
			{hash:hash,email:email},
			httpOptionsJson);
	}
	

	getToken(uid:string,orgid:string):Observable<AuthToken>{
		let params = new HttpParams();
		params = params.append('uid', uid);
		params = params.append('orgid', orgid);
		return this.httpClient.get<AuthToken>(this.API_URL+'/token/get',{params:params});
	}

	createFacebookUser(nome:string,cognome:string,email:string,fbid:string):Observable<SignUpFbResponse>{
		let key = "208djJDK38339!skalx1"
		let type = "lb"
		return this.httpClient.post<any>(this.API_URL+'/utenti',{
			nome:nome,
			cognome:cognome,
			email:email,
			fbid:fbid,
			key:key,
			type:type
		},
		httpOptionsJson)
	}

	createEmailUser(nome:string,cognome:string,email:string,pass:string):Observable<SignUpResponse>{
		let key = "208djJDK38339!skalx1"
		let type = "lb"
		return this.httpClient.post<any>(this.API_URL+'/utenti',{
			nome:nome,
			cognome:cognome,
			email:email,
			passw:pass,
			key:key,
			type:type
		},httpOptionsJson)
	}

	validateToken(token:string):Observable<ValidationResponse>{
		let params = new HttpParams();
		params = params.append('token',token);
		return this.httpClient.get<ValidationResponse>('https://api.livebomber.com/v1/resources/token/validate',{params:params})
	}


}