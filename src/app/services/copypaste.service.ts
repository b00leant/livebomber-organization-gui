import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class CopypasteService {

	constructor() { }

	private copySource = new Subject<string>();

	copy$ = this.copySource.asObservable();

	copy(message:string) {
		this.copySource.next(message);
	}
}
