import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Subject }    from 'rxjs';
import { LoginResponse } from '../models/login-response';
import { UserInfo } from '../models/user-info';

interface myData {
	success: boolean,
	message: string
}
interface Credentials {
	username:string,
	password:string
}


@Injectable({
	providedIn: 'root'
})
export class LBAuthService {

	myCred:Credentials = {
		username:"admin",
		password:"admin"
	};


	constructor(
		private http:HttpClient) {
	}
	private signInSource = new Subject<UserInfo>();
	private signOutSource = new Subject<void>();
	private signInWithFBSource = new Subject<void>();
	private loggedInStatus = false;
	private mustSelectOrg = false;

	signIn$ = this.signInSource.asObservable();
	signOut$ = this.signOutSource.asObservable();
	signInWithFB$ = this.signInWithFBSource.asObservable();

	private user_id:string="";
	private email_address:string=""
	private first_name:string="";
	private last_name:string="";

	setUser(nome:string,cognome:string,id:string,email:string){
		this.user_id = id;
		this.email_address = email
		this.first_name = nome
		this.last_name = cognome
		let infos:UserInfo = {
			id:id,
			nome:nome,
			cognome:cognome,
			email:email,
			privacy:0
		}
		this.signInSource.next(infos);
	}

	setLoggedIn(value: boolean) {
		this.loggedInStatus = value
	}

	get isLoggedIn() {
		return this.loggedInStatus
	}

	get id(){
		return this.user_id;
	}

	get fName(){
		return this.first_name;
	}
	
	get lName(){
		return this.last_name;
	}

	get email(){
		return this.email_address
	}

	getUserDetails(username, password):Observable<LoginResponse> {
		let headers: HttpHeaders = new HttpHeaders();
		headers.append('Content-Type','application/x-www-form-urlencoded');
		headers.append('Content-Type','application/json');
		headers.append('Access-Control-Allow-Headers','content-type');
		headers.append('Access-Control-Allow-Origin','*');
		return this.http.post<LoginResponse>('https://api.livebomber.com/v1/resources/utenti/login', {
			email: username,
			passw: password,
		},{headers})
	}

	signInWithFB(){
		this.signInWithFBSource.next();
	}

	/*

	OLD AUTH METHODS

	signIn() {
		this.signInSource.next();
	}

	signOut() {
		this.signOutSource.next();
	}
	*/
	

}
