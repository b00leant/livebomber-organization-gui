import { TestBed } from '@angular/core/testing';

import { CopypasteService } from './copypaste.service';

describe('CopypasteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CopypasteService = TestBed.get(CopypasteService);
    expect(service).toBeTruthy();
  });
});
