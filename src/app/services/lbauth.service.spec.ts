import { TestBed } from '@angular/core/testing';

import { LBAuthService } from './lbauth.service';

describe('LBAuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LBAuthService = TestBed.get(LBAuthService);
    expect(service).toBeTruthy();
  });
});
