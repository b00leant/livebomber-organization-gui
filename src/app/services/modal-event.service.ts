import { Injectable } from '@angular/core';
import { VideoModalData } from '../video-modal/videoModalData'
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalEventService {

  constructor() { }

  private modalEventSource = new Subject<VideoModalData>();

  modalShareEvent$ = this.modalEventSource.asObservable();

  launchShareEvent(data:VideoModalData){
  	this.modalEventSource.next(data);
  }
}
