import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';
import { LBAuthService } from '../services/lbauth.service';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

interface myData {
  message: string,
  success: boolean
}

interface isLoggedIn {
  status: boolean
}

interface logoutStatus {
  success: boolean
}
@Injectable()
export class UserService {

  constructor(
    private cookie:CookieService,
    private auth:LBAuthService,
    private http:HttpClient) {    
  }

  getSomeData() {
    return this.http.get<myData>('https://api.livebomber.com/database.php')
  }

  isLoggedIn(){
    return this.http.get<any>('https://api.livebomber.com/isloggedIn.php')
  }

  logout() {
    this.auth.setLoggedIn(false)
    //this.cookie.delete('lbac2k19');
    localStorage.removeItem('lbac2k19')
    return true;
  }

}