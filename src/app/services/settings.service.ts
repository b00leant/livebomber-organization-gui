import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';
import { Organization } from '../models/organization';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {


	private organizationChosenSource = new Subject<Organization>();

	organizationChosen$ = this.organizationChosenSource.asObservable();

	setOrganization(org: Organization) {
    this.organizationChosenSource.next(org);
  }



  constructor() { }
}