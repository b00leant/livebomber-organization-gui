import { TestBed } from '@angular/core/testing';

import { ModalEventService } from './modal-event.service';

describe('ModalEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModalEventService = TestBed.get(ModalEventService);
    expect(service).toBeTruthy();
  });
});
