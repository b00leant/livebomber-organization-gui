import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {FormControl, Validators, FormGroup,FormBuilder, ValidatorFn, ValidationErrors} from '@angular/forms';
import { Router } from '@angular/router';
import { LBAuthService } from '../services/lbauth.service';
import { APIService } from '../services/api.service';
import { CookieService } from 'ngx-cookie-service';


interface myData {
  success: boolean,
  message: string
}

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {

	constructor(
    private cookieService: CookieService,
    private router:Router,
    private auth:LBAuthService,
    private _loc:Location,
    private _api:APIService,
    private _formBuilder:FormBuilder) { }

	formControl = new FormControl('', [Validators.required, Validators.email]);

  signinGroup: FormGroup;

  valid:boolean = false;
  authError:boolean= false;

  goBack(){
    this._loc.back();
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
    this.email.hasError('email') ? 'Not a valid email':
    this.email.hasError('invalidCredentials') ? 'Invalid email or password..':''

  }

  onEmailInput(){
    if(this.signinGroup.valid){
      this.valid = true;
    }else{
      this.valid = false;
    }
  }

  onPasswordInput(){
    if(this.signinGroup.valid){
      this.valid = true;
    }else{
      this.valid = false;
    }
  }

  /* Shorthands for form controls (used from within template) */
  get password() { return this.signinGroup.get('password'); }
  get email() { return this.signinGroup.get('email'); }

  signInUser(event) {
    event.preventDefault()
    const target = event.target
    const username = target.querySelector('#email').value
    const password = target.querySelector('#password').value
    if(this.valid){
      return this.auth.getUserDetails(username, password).subscribe(data => {
        console.log(data)
        if(data.result) {
          console.log("Authentication: OK");
          localStorage.setItem('lblu2k19',data.userid);
          this._api.getUser(data.userid).subscribe(user=>{
            console.log(user)
            this.auth.setUser(user.nome,user.cognome,user.id,user.email);
            this.router.navigate(['selection'])
          })
        } else {
          console.log("Authentication: FAILED")
          this.authError= true;
          console.log(this.password);
          target.querySelector('#email').value = "";
          target.querySelector('#password').value = "";
          //this.password.setErrors([{'invalidCredentials': true}]);
        }
      })
    }
  }

  ngOnInit() {
    this.signinGroup = this._formBuilder.group({
      password:['',[Validators.required]],
      email: ['',[Validators.required]]
    });
  }
}

