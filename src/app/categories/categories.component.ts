import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { League } from '../models/league';
import { Category } from '../models/category';
import { Group } from '../models/group';
import { APIService } from '../services/api.service';
import { SettingsService } from '../services/settings.service';
import { ActivatedRoute,Router,NavigationEnd,Event,ParamMap} from '@angular/router';
import { switchMap} from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { Organization } from '../models/organization';
import { GroupContainer } from '../models/groupContainer';

@Component({
	selector: 'app-categories',
	templateUrl: './categories.component.html',
	styleUrls: ['./categories.component.scss'],
	//changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesComponent implements OnInit {

	panelOpenState = false;
	organizationID:string;
	WS:boolean;
	subscription: Subscription;

	default_url:string = "https://league.livebomber.com/public/uploads/tornei/torneiPH.png";
	
	org: Organization;
	groups: Group[];
	groupsCont: GroupContainer[] = [];


	leagues: League[] = [
	{
		name: "fortitudo"
	},
	{
		name: "alessandrino"
	}
	];

	loaded:boolean = false;

	tipi:string[] = [];

	constructor(
		private _api: APIService,
		private _ar: ActivatedRoute,
		private _settings: SettingsService,
		private _ro: Router) {
	}

	ngOnInit() {
		let id = this._ar.parent.snapshot.paramMap.get('id');
		this.organizationID = id;
		this.getOrgCategories(id);
	}

	getOrgCategories(id:string){
		return this._api.getOrg(id).subscribe(org=>{
			this.loaded = true;
			this.org = org[0];
			if(org[0].display_ws == "1"){
				this.WS = true;
				this.getMergedGroups(id);
			}else{
				this.WS = false;
				this.getGroups(id);
			}
		});
	}

	isWS(){
		if(this.org.display_ws == "1"){
			return true;
		}else{
			return false;
		}
	}

	
	goToGroup(g:Group){
		this._ro.navigate(['league/'+g.id,{title:g.nome}]);
	}

	groupURL(id:string){
		return "league/"+id;
	}

	getGroups(id:string){
		return this._api.getOrgGroups(id).subscribe(data=>{
			this.groups = data;
		});
	}

	getMergedGroups(id:string){
		return this._api.getOrgGroups(id).subscribe(data=>{
			this.groups = data;
			data.forEach( (g:Group) => {
				if(!this.tipi.includes(g.tipo) && g.tipo !=""){
					this.tipi.push(g.tipo)
				}
			});
			this.tipi.forEach((t)=>{
				this.groupsCont.push({title:t,groups:[]});
			});
			this.groupsCont.forEach((gc:GroupContainer)=>{
				this.groups.forEach((g:Group)=>{
					if(g.tipo == gc.title){
						gc.groups.push(g);
					}
				});
			});
		});
	}

}
