import { Component, OnInit, ViewChild } from '@angular/core';
import { Person } from '../models/person';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { MatTable, MatSort } from '@angular/material';
import { MatSliderChange } from '@angular/material/slider';
import { Router, ActivatedRoute } from '@angular/router';
import { APIService } from '../services/api.service';
import { Organization } from '../models/organization';
import { LeagueDay } from '../models/leagueDay';
import { LeagueCalendar } from '../models/leagueCalendar';
import { Classification } from '../models/classification';
import { Team } from '../models/team';
import { Match } from '../models/match';


const CANNONIERI: Person[] = [
{name: "Francesco Totti", goal: 43, url_img: 'https://futhead.cursecdn.com/static/img/14/players/1238.png'},
{name: "Gonzalo Higuain", goal: 63, url_img: 'https://futhead.cursecdn.com/static/img/14/players/167664.png'},
{name: "Daniele De Rossi", goal: 22, url_img: 'https://futhead.cursecdn.com/static/img/19/players/53302.png'},
{name: "Diego Armando Maradona", goal: 143, url_img: 'https://futhead.cursecdn.com/static/img/18/players_worldcup/190042.png'},
];


@Component({
	selector: 'app-league',
	templateUrl: './league.component.html',
	styleUrls: ['./league.component.scss']
})


export class LeagueComponent implements OnInit {

	calIndex = 0;

	prevCal(){
		if(this.calIndex > 0){
			this.calIndex--;
			this.calendarSource = this.calendarDays[this.calIndex].partite;
		}
	}

	nextCal(){
		if(this.calIndex < this.calendarDays.length){
			this.calIndex++;
			this.calendarSource = this.calendarDays[this.calIndex].partite;
		}
	}

	onSlideChange(event: MatSliderChange){
		this.calendarSource = this.calendarDays[event.value -1].partite;
		//this.calendarDays[0][event.value.toString()];
		//console.log(this.calendarDays[0][event.value.toString()]);
		//console.log(Object.keys(this.calendarDays[0][1]));
		/*if(event.value < 6){
			this.calendarTable.dataSource = CAL2;
			this.calendarTable.renderRows();
		}else{
			this.calendarTable.dataSource = CAL1;
			this.calendarTable.renderRows();
		}*/
	}

	@ViewChild('calTable') private calendarTable: MatTable<LeagueDay>;

	calendarDays: LeagueDay[] =[];

	calendarSource: Match[] = [];
	classificationSource: Classification[] = [];
	teamsSource: Team[] = [];

	default_url:string = "https://league.livebomber.com/public/uploads/tornei/torneiPH.png";

	/* Slider params */

	autoTicks = false;
	disabled = false;
	invert = true;
	min:number = 0
	showTicks = true;
	step = 1;
	thumbLabel = true;
	value = 1;
	vertical = false;
	max:number = 0;
	org:Organization;
	WS:boolean;

	/* Progress Spinner params */

	loaded:boolean = false;

	get tickInterval(): number | 'auto' {
		return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
	}
	set tickInterval(value) {
		this._tickInterval = coerceNumberProperty(value);
	}
	private _tickInterval = 1;

	// Setto le colonne di ogni tabella con array di stringhe

	displayedColumnsTeam: string[] = ['url_img','name', 'points'];

	displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];

	displayedColumnsBombers: string[] = [
	'url_img',
	'name',
	'goals'
	];

	displayedColumnsClassification: string[] = [
	//'pos',
	'url_img',
	'sq',
	'gf',
	'gs',
	'v',
	'pa',
	'pe',
	'giocate',/*
	'diffReti',
	'punti'*/
	];

	displayColumnsMatchesCal: string[] = [
	"sq1_img",
	"sq1",
	"ris1",
	"sq2",
	"sq2_img"
	];

	dataBombers = CANNONIERI


	constructor(private _ar: ActivatedRoute,private _api:APIService) {}

	getOrg(id:string){
		this._api.getOrg(id).subscribe(org =>{
			this.loaded=true;
			this.org = org[0];
			if(org[0].display_ws == "1"){
				this.WS = true;
				this.getCal(this._ar.snapshot.paramMap.get('id'));
				this.getClassification(this._ar.snapshot.paramMap.get('id'));
				this.getTeam(this._ar.snapshot.paramMap.get('id'));
			}else{
				this.WS = false;
				this.getTeam(this._ar.snapshot.paramMap.get('id'));
			}
		});
	}

	getCal(id:string){
		this._api.getLeagueCalendar(id).subscribe(cal=>{
			//console.log(cal);
			//console.log(Object.keys(cal));
			this.max = Object.keys(cal).length;
			this.min = 1;
			//this.calendarDays = cal;
			console.log(cal);
			this.calendarDays = cal;
			console.log(cal[0].partite);
			this.calendarSource = cal[0].partite;
		});
	}	

	getClassification(id:string){
		this._api.getLeagueClassification(id).subscribe(cla=>{
			/*cla.forEach((c:Classification)=>{
				c.sq_url_img = "https://www.livebomber.com/up/uploads-sito/loghi/"+c.id+".png"
				this.classificationSource.push(c);
			});*/
			this.classificationSource = cla;
		});
	}

	getTeam(id:string){
		this._api.getLeagueTeams(id).subscribe(t=>{
			this.teamsSource = t;
			console.log("SQUADRE..");
			console.log(t);
		});
	}

	ngOnInit() {
		let id = this._ar.parent.snapshot.paramMap.get('id');
		this.getOrg(id);
		let leagueID = this._ar.snapshot.paramMap.get('id');

	}


}