	import { Organization } from '../models/organization';
	import { Player } from '../models/player'
	import { Team } from '../models/team'
	import { Pipe, PipeTransform } from '@angular/core';
	@Pipe({
		name: 'match_filter'
	})
	export class FilterPipe implements PipeTransform {
		transform(items: Organization[], searchText: string): Organization[] {
			if(!items) return [];
			if(!searchText) return items;
			searchText = searchText.toLowerCase();
			return items.filter( it => {
				return it.title.toLowerCase().includes(searchText);
			});
		}
	}


	@Pipe({
		name: 'org_filter'
	})
	export class OrgFilter implements PipeTransform {
		transform(items: Organization[], searchText: string): Organization[] {
			if(!items) return [];
			if(!searchText) return items;
			searchText = searchText.toLowerCase();
			return items.filter( it => {
				return (it.title.toLowerCase().includes(searchText) && searchText.length>0);
			});
		}
	}

	@Pipe({
		name: 'players_filter'
	})
	export class PlayersFilter implements PipeTransform {
		transform(items: Player[], searchText: string): Player[] {
			if(!items) return [];
			if(!searchText) return items;
			searchText = searchText.toLowerCase();
			return items.filter( it => {
				return ((it.nome.toLowerCase().includes(searchText)||
					it.cognome.toLowerCase().includes(searchText))
					&& searchText.length>0);
			});
		}
	}