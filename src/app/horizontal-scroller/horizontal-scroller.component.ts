import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
	selector: 'lb-hor-scroller',
	templateUrl: './horizontal-scroller.component.html',
	styleUrls: ['./horizontal-scroller.component.css']
})
export class HorizontalScrollerComponent implements OnInit {

	@Output() selected = new EventEmitter<string>();

	s_p:boolean = true;
	s_t:boolean;
	s_m:boolean;
	s_g:boolean;

	p:string = "pla";
	t:string = "tea";
	m:string = "mat";
	g:string = "gro";

	constructor() { }

	changeStatus(v:string){
		this.s_p=false;
		this.s_t=false;
		this.s_m=false;
		this.s_g=false;
		switch (v) {
			case "pla":
			this.s_p = true;
			break;
			case "tea":
			this.s_t = true;
			break;
			case "mat":
			this.s_m = true;
			break;
			case "gro":
			this.s_g = true;
			break;
			default:
			// code...
			break;
		}
	}

	select(v:string){
		console.log(v);
		this.changeStatus(v);
		this.selected.emit(v);
	}

	ngOnInit() {
	}

}
