import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleVideoPageComponent } from './single-video-page.component';

describe('SingleVideoPageComponent', () => {
  let component: SingleVideoPageComponent;
  let fixture: ComponentFixture<SingleVideoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleVideoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleVideoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
