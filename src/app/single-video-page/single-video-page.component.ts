import { Component, OnInit } from '@angular/core';
import { APIService } from '../services/api.service';
import { Action } from '../models/action';
import { ActivatedRoute } from '@angular/router';
import { Player } from '../models/player';
import { Meta } from '@angular/platform-browser';

@Component({
	selector: 'app-single-video-page',
	templateUrl: './single-video-page.component.html',
	styleUrls: ['./single-video-page.component.css']
})
export class SingleVideoPageComponent implements OnInit {

	player:Player;
	action:Action;

	constructor(
		private _api:APIService,
		private _ar:ActivatedRoute,
		private meta: Meta) {
	}

	ngOnInit() {
		this.action = this._ar.snapshot.data['action'][0];
		this.player = this._ar.snapshot.data['player'][0];
		console.log(this._ar.snapshot.data);
		this.meta.addTag({ property: 'og:title', content:'Livestory di '+this.player.nome+' '+this.player.cognome});
		this.meta.addTag({ property: 'og:video:url', content: this.action.url });
		this.meta.addTag({ property: 'og:video:secure_url', content: this.action.url });
		this.meta.addTag({ property: 'og:image', content: this.action.thumb });
		this.meta.addTag({ property: 'og:video:type', content: 'video/mp4' });
		this.meta.addTag({ property: 'og:video:height', content: '720' });
		this.meta.addTag({ property: 'og:video:width', content: '1280' });
		this.meta.addTag({ property: 'og:type', content:'video.other'});
		this.meta.addTag({ property: 'og:description', content:'Questo è un video di sharing test di Live Bomber!'});
		this.meta.addTag({ property: 'og:url', content: 'https://live-bomber.herokuapp.com/video/'+this.action.id});
	}

}
