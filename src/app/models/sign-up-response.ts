export class SignUpResponse{
	result:string;
	errcode:string;
	message:string;
	id:string;
	newuser:boolean;
}