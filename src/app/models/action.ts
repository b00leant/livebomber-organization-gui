export class Action{
	id:string;
	likes:string;
	views_u:string;
	views:string;
	score:string;
	preferito:string;
	giocatore:string;
	tag:string;
	tag_title:string;
	organizzazione:string;
	livestory:string;
	thumb?:string;
	url?:string;
}