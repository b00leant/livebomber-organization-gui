import { League } from './league';

export class Category{
	id: string;
	leagues: League[];
}	