import { Team } from './team';


export class Match {


	sq1: string;
	sq1id: string;
	sq2: string;
	sq2id: string;
	ris1: string;
	ris2: string;
	status?: string;
	data?: Date;
	giornata?: string;	
	re?:string;
	lsid?:string;

	//h_team: Team;
	//g_team: Team;
}