export class Classification{
	sq_url_img:string;
	sq: string;
	id: string;
	gf: string;
	gs: string;
	v: string;
	pa: string;
	pe: string;
	giocate: string;
	diffReti: string;
	punti: string;
}