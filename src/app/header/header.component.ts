import { Component, OnInit, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { APIService } from  '../services/api.service';
import { Organization } from '../models/organization';
import { Observable } from 'rxjs';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	constructor(private api: APIService) { }

	org: Organization;
	imgURL:string="../assets/logolb.svg";
	title:string="";
	tagName:string="";
	loaded:boolean = false;


		@Input() categoriesURL:string;
		@Input() titleInput:string;	
		@Input() backNav:string;

	getOrg(id:string) {
		return this.api.getOrg(id).subscribe(data => {
			this.org = data[0];
			this.imgURL = "https://league.livebomber.com/public/uploads/tornei/"+data[0].id+".png";
			this.title = data[0].title;
			this.tagName = data[0].title.toLowerCase().split(' ').join('');
			this.loaded = true;
		});
	}

	ngOnInit() {
	}

}
