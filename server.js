//Install express server
const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();

// Serve only the static files form the dist directory



app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, content-type, Content-Type, Accept");
	res.setHeader('Access-Control-Allow-Credentials', true);
	/*
	response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");	
	*/
	next();
});

//app.use(cors());
app.use(express.static('./dist/browser'));

app.get('/*', function(req,res) {

	res.sendFile(path.join(__dirname,'/dist/browser/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);